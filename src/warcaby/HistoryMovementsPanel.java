/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcaby;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author Arkadio
 */
public class HistoryMovementsPanel extends JPanel{
    Color black = new Color(0, 120, 0);
    Color white = new Color(255, 255, 128);
    
    CheckerBoard checkerboard;
    Principle rules;
    private final int boardSize;
    private final int fieldSize;
            
    HistoryMovementsPanel(int boardSize, int fieldSize)
    {
        this.fieldSize = fieldSize;
        this.boardSize = boardSize;
        checkerboard = new CheckerBoard(boardSize, fieldSize);
        rules = new Principle(checkerboard.checkerboard, boardSize, fieldSize);
        rules.createCheckers();
    }
    
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        checkerboard.drawBoard(g);
        
        for(Checker k: rules.getBlacks())
        {
            g.setColor(black);  
            k.drawChecker(g, fieldSize);
        }
        
        for(Checker k: rules.getWhites())
        {
            g.setColor(white);
            k.drawChecker(g, fieldSize);
        }
        
    }
    
    @Override
    protected void finalize() throws Throwable {
        super.finalize();                
    }
}
