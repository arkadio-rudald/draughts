/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcaby;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import javax.swing.JPanel;
import warcaby.NetworkConnection.ErrorHandling;

/**
 * ControlGame is a JPanel class which has Mouse listeners of our checkerboard.
 * <br /><br />
 * When you PRESS the mouse program checks:
 * <br /><br />
 * 
 * 1. Which team begins and after we blockade the enemy team, in this way that you can't
 * move the enemy checker.
 * <br />
 * 2. Which checker is marked. In case of no-marked checker do nothing
 * <br />
 * 3. Is checker in the state of war with enemy draughtsman
 * <br /><br />
 * 
 * When you DRAG the mouse program does:
 * <br /><br />
 * 
 * 
 * <br /><br />
 * When you RELEASE the mouse program does:
 * <br />
 * 1. In case of dragged mouse move from Field A to Field A we redraw the checker.
 * <br />
 * 2. In other cases program checks if the marked checker has any battle then program removes all of taken checkers.
 * If not, checks - is Field destinated free and is it possible to move.
 *
 * 3. Check if is it end of game.
 *
 * <br /><br />
 * 
 * Variables:
 * <br />
 * boolean blackfirst - indicates which checker team begins.
 * <br />
 * lastChecker
 * <br />
 * markedField - we need to know, where the checker has to move
 * <br />
 * lastField - last position of checker (used in case of battle)
 * <br />
 * 
 * boolean wasClicked - is needed to avoid the exception:
 * <br />
 * When you click on checker and you have a battle, you move (without drag) to another point,
 * you press the mouse, drag, and release in the same place.
 * @author Arkadio
 */
public class ControlGame extends JPanel implements MouseListener, MouseMotionListener {

    private Color black = new Color(0, 120, 0);
    private Color white = new Color(255, 255, 128);

    private boolean blackFirst = false;
    private Checker lastChecker;
    private Field markedField;
    private Field lastField;
    private boolean wasBattle;
    private boolean wasClicked;
    private boolean isMouseDragged;
    boolean noBattleAfterBecomeAKing = false;
    
    CheckerBoard checkerboard;
    Principle rules;
    
    boolean testDragged = false;  
    
    HistoryMovements historyGame;
    private int boardSize;
    private int fieldSize;
    
    ControlGame(int boardSize, int fieldSize)
    {
        this.fieldSize = fieldSize;
        this.boardSize = boardSize;
        checkerboard = new CheckerBoard(boardSize, fieldSize); 
        
        historyGame = new HistoryMovements(boardSize, fieldSize);
        rules = new Principle(checkerboard.getBoard(), boardSize, fieldSize);
        historyGame.getCheckerList().add(historyGame.saveInCheckerLists(rules.getWhites(), rules.getBlacks()));                   
    }
    
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        
        checkerboard.drawBoard(g);
        if(!blackFirst)
        {
            for(Checker k: rules.getBlacks())
            {
                g.setColor(black);
                k.drawChecker(g, fieldSize);
            }
           
            for(Checker k: rules.getWhites())
            {
                g.setColor(white);
                k.drawChecker(g, fieldSize);
            }        
        }
        else
        {
            for(Checker k: rules.getWhites())
            {
                g.setColor(white);
                k.drawChecker(g, fieldSize);
            }
            for(Checker k: rules.getBlacks())
            {
                g.setColor(black);
                k.drawChecker(g, fieldSize);
            }            
        }
    }
    

    void rotateBoard(boolean czarny)
    {
    }

        
    @Override
    public void mouseClicked(MouseEvent e) {
    }   

    @Override
    public void mousePressed(MouseEvent e) {
        
        //sprawdzenie i zablokowanie pionków drużyny przeciwnej
        if(blackFirst)
        {
            rules.blockadeCheckers(rules.getWhites());
            rules.activateCheckers(rules.getBlacks());
        }
        else
        {
            rules.blockadeCheckers(rules.getBlacks());
            rules.activateCheckers(rules.getWhites());
        }
            
        lastField = markedField;
        markedField = rules.checkField(e.getX(), e.getY());
        
        //sprawdzam czy na polu znajduje się pionek
        if(rules.getCheckerFromField(markedField) != null)
            rules.setMarkedChecker(rules.getCheckerFromField(markedField));        
        
        //jeśli istnieje pionek zaznaczony to sprawdzamy czy 
        //ma bicie 
        
        if(rules.getMarkedChecker() != null)
        {
            System.out.println("Mouse Pressed Checker: " + rules.getMarkedChecker().getId() + " Field: " + markedField.getId());
            rules.blockadeFreeFields();
            
            if(!rules.isBattle(blackFirst).isEmpty() && !noBattleAfterBecomeAKing)
            {
                wasBattle = true;
                if(lastChecker != null)
                {
                    if(rules.getMarkedChecker().getId() != lastChecker.getId())
                        rules.blockadeFreeFields();
                }                
            }
            else
            {                
                rules.activateAllFreePossiblyFields();
                rules.blockadeImpossiblyFieldToPutChecker(rules.getMarkedChecker());                
            }            
        }
        else
            System.out.println("Mouse Pressed Checker: null " + " Field: " + markedField.getId());
        
        if(lastField == null)
            lastField = new Field();
        
        testDragged = false;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        testDragged = false;
        if(isMouseDragged)
        {
            System.out.println("Mouse Released Checker: " + rules.getMarkedChecker().getId() + " Field: " + rules.checkField(e.getX(), e.getY()).getId());
            System.out.println("--------------");
            
            Field theSameField = rules.getMarkedChecker().getField();            
            if((rules.getMarkedChecker().getField().getId() == rules.checkField(e.getX(), e.getY()).getId()
                    && !markedField.isForbidden() && rules.checkField(e.getX(), e.getY()).isFree()))
            {
                rules.getMarkedChecker().setToDrawX(rules.getMarkedChecker().getField().getX1());
                rules.getMarkedChecker().setToDrawY(rules.getMarkedChecker().getField().getY1());
                
                repaint();
            }
            else
            {
                if(rules.getMarkedChecker() != null)
                {

                    if(rules.isCorrectField(rules.checkField(e.getX(), e.getY()), rules.getMarkedChecker()))
                    {
                        rules.getMarkedChecker().getField().setX1(rules.checkField(e.getX(), e.getY()).getX1());
                        rules.getMarkedChecker().getField().setY1(rules.checkField(e.getX(), e.getY()).getY1());
                        rules.getMarkedChecker().setToDrawX(rules.checkField(e.getX(), e.getY()).getX1());
                        rules.getMarkedChecker().setToDrawY(rules.checkField(e.getX(), e.getY()).getY1());

                        rules.activateAllFreePossiblyFields();
                        markedField.setFree(true);
                        markedField.setForbidden(false);
                        
                        if(!wasClicked)
                            lastField = markedField;
                        
                        if(!isMouseDragged)
                            rules.setMarkedChecker(rules.getCheckerFromField(markedField));            
                        
                        // Sprawdzenie czy obecny pionek ma bicie
                        if(rules.isBattle(rules.getMarkedChecker()) && wasBattle)
                        {
                            markedField = rules.checkField(e.getX(), e.getY());
                            if(wasBattle)
                            {
                                try{
                                    rules.removeTakenChecker(rules.getMarkedChecker(), markedField, lastField);
                                }
                                catch(NullPointerException es)
                                {
                                    try {
                                        ErrorHandling errorCall = new ErrorHandling();
                                        errorCall.sendPOST(errorCall.getErrorDetails(es));
                                    } catch (IOException ex1) {
                                    }
                                }
                            }
                            lastChecker = rules.getMarkedChecker();      
                            lastField = markedField;          
                            repaint();                           
                        }
                        lastField = markedField;
                        
                        rules.activateAllFreePossiblyFields();
                        
                        //Check if the checker is becoming to be a king
                        noBattleAfterBecomeAKing = rules.getMarkedChecker().isKing();
                        
                        rules.isCheckerToBeAKing(rules.getMarkedChecker(), wasBattle);
                        if(rules.getMarkedChecker().isKing() != noBattleAfterBecomeAKing)
                            noBattleAfterBecomeAKing = true;
                        else
                            noBattleAfterBecomeAKing = false;
                        
                        if(isMouseDragged)
                        {
                            rules.checkField(e.getX(), e.getY()).setForbidden(false);
                            rules.checkField(e.getX(), e.getY()).setFree(false);
                        }
                        
                        if(!noBattleAfterBecomeAKing)
                        {   //Sprawdzenie czy ten sam pionek ma kolejne bicia
                            if(!(rules.isBattle(rules.getMarkedChecker()) && wasBattle))
                            {
                                lastChecker = null;
                                rules.setMarkedChecker(null);     
                                wasBattle = false;
                                blackFirst = !blackFirst;
                            }
                        }
                        else
                        {
                            lastChecker = null;
                            rules.setMarkedChecker(null);     
                            wasBattle = false;
                            blackFirst = !blackFirst;
                        }
                        repaint();   
                    }
                    else
                    {
                        rules.getMarkedChecker().setToDrawX(rules.getMarkedChecker().getField().getX1());
                        rules.getMarkedChecker().setToDrawY(rules.getMarkedChecker().getField().getY1());                        
                        rules.setMarkedChecker(null);
                        repaint();
                    }
                }         
                if(theSameField.getId() != rules.checkField(e.getX(), e.getY()).getId())
                    historyGame.getCheckerList().add(historyGame.saveInCheckerLists(rules.getWhites(), rules.getBlacks()));                               
            }    
            wasClicked = false;
        }   
        else
        {
            if(rules.getMarkedChecker() != null)
            {
                if(rules.isCorrectField(rules.checkField(e.getX(), e.getY()), rules.getMarkedChecker()))
                {
                    wasClicked = true;
                    System.out.println("Mouse Released Checker: " + rules.getMarkedChecker().getId() + " Field: " + rules.checkField(e.getX(), e.getY()).getId());
                    System.out.println("--------------");   

                    rules.activateAllFreePossiblyFields();
                    markedField.setFree(false);
                    markedField.setForbidden(false);
                    rules.setMarkedChecker(rules.getCheckerFromField(markedField));            
                    
                    if(wasBattle)
                        rules.removeTakenChecker(rules.getMarkedChecker(), markedField, lastField);
                    
                    rules.activateAllFreePossiblyFields();
                    if(rules.isBattle(rules.getMarkedChecker()) && wasBattle)
                    {
                        lastChecker = rules.getMarkedChecker();      
                        lastField = markedField;          
                    }
                    else
                    {
                        noBattleAfterBecomeAKing = rules.getMarkedChecker().isKing();
                        rules.isCheckerToBeAKing(rules.getMarkedChecker(), wasBattle);
                    
                        if(rules.getMarkedChecker().isKing() != noBattleAfterBecomeAKing)
                            noBattleAfterBecomeAKing = true;
                        else
                            noBattleAfterBecomeAKing = false;
                                                
                        lastChecker = null;
                        rules.setMarkedChecker(null);     
                        wasBattle = false;
                        blackFirst = !blackFirst;
                        wasClicked = false;
                    }
                    historyGame.getCheckerList().add(historyGame.saveInCheckerLists(rules.getWhites(), rules.getBlacks()));                               
                    repaint();
                }
                else if(rules.getMarkedChecker().getField().getId() == rules.checkField(e.getX(), e.getY()).getId())
                    wasClicked = true;
                else
                {
                    markedField = lastField;
                    wasClicked = false;
                    System.out.println("Mouse Released Checker: null " + " Field: " + rules.checkField(e.getX(), e.getY()).getId());
                    System.out.println("--------------");

                }
            }
        }        
        isMouseDragged = false;        
        
        //warunek końca
        if(rules.getWhites().isEmpty() || rules.getBlacks().isEmpty())
        {
            if(!rules.getWhites().isEmpty())
            {
                //Pokaż okno dialogowe białe wygrały
            }
            else
            {
                //Pokaż okno dialogowe czarne wygrały
            }
              
            try {
                historyGame.saveMovementsToFile(historyGame.findMovesInMovementsLists(), true);
                //KONIEC
                //zapisujemy historię, wskazujemy wygranego
            } catch (FileNotFoundException ex) {
                try {
                    ErrorHandling errorCall = new ErrorHandling();
                    errorCall.sendPOST(errorCall.getErrorDetails(ex));
                } catch (IOException ex1) {
                }
            }
        }          
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if(!testDragged)
            System.out.println("Mouse Dragged");
        if(rules.getMarkedChecker() != null)
        {
            //STAŁA
            rules.getMarkedChecker().setToDrawX(e.getX()-(fieldSize)/2);
            rules.getMarkedChecker().setToDrawY(e.getY()-(fieldSize)/2);
            repaint();
            isMouseDragged = true;
        }
        testDragged = true;
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        //System.out.println("Mouse Moved");
    }
    
    
    /**
     * Loading Game from txt file:
     * first line: "Is black next?" true or false
     * next lines are checkers in this order
     * isBlack, CheckerId, isKing, FieldId, 
     * 
     * 
     * @param file
     * @throws FileNotFoundException 
     */
    
    
    public void loadGame(File file) throws FileNotFoundException
    {
        
        //dodać obiekty do listy ruchów z pliku unfinished
        //parsowanie
        Scanner in = new Scanner(file);
        boolean whosFirst = Boolean.valueOf(in.nextLine());
        
        char[] filename = file.getPath().toCharArray();
        
        filename[filename.length-4] = 'M';
        filename[filename.length-3] = 'o';
        filename[filename.length-2] = 'v';
        filename[filename.length-1] = 'e';
        
        
        String flname = String.valueOf(filename);
        flname = flname.concat("s.txt");
        
        
        File fileHistory = new File(flname);
        historyGame.setMovementsToSave(historyGame.parseMoveToString(historyGame.readMovementsFromFileToMove(fileHistory)));
        
        
        List <Checker> blackCheckers = new ArrayList();
        List <Checker> whiteCheckers = new ArrayList();
        
        do
        {
            String[] checkerData = in.nextLine().split(" ");
            Checker checker = new Checker();
            checker.setBlack(Boolean.valueOf(checkerData[0]));
            checker.setId(Integer.valueOf(checkerData[1]));
            checker.setKing(Boolean.valueOf(checkerData[2]));
            checker.setField(rules.getFieldById(Integer.valueOf(checkerData[3])));
            
            //Setting free field is embded in setField function
            //rules.getFieldById(Integer.valueOf(checkerData[3])).setFree(false);

            
            if(whosFirst)
            {
                if(!checker.isBlack())                
                    rules.getFieldById(Integer.valueOf(checkerData[3])).setForbidden(true);                                
            }
            else
            {
                if(checker.isBlack())
                    rules.getFieldById(Integer.valueOf(checkerData[3])).setForbidden(true);                
            }           
                    
            if(checker.isBlack())            
                blackCheckers.add(checker);            
            else
                whiteCheckers.add(checker);                        
        }
        while(in.hasNext());
        
        blackFirst = whosFirst;
        rules.setBlacks(blackCheckers);
        rules.setWhites(whiteCheckers);
        rules.changeFieldsToFreeIfEmpty();
        repaint();       
    }    
    
    public boolean isFileCorrect(File file)
    {
        boolean result = false;
        
        try
        {
            Scanner in = new Scanner(file);
            boolean whosFirst = Boolean.valueOf(in.nextLine());
            String[] checkerData = in.nextLine().split(" ");
            Checker checker = new Checker();
            checker.setBlack(Boolean.valueOf(checkerData[0]));
            checker.setId(Integer.valueOf(checkerData[1]));
            checker.setKing(Boolean.valueOf(checkerData[2]));
            checker.setField(rules.getFieldById(Integer.valueOf(checkerData[3])));
            
            result = true;                    
        }
        catch(FileNotFoundException | NullPointerException | NumberFormatException | NoSuchElementException e)
        {
            try {
                ErrorHandling errorCall = new ErrorHandling();
                errorCall.sendPOST(errorCall.getErrorDetails(e));
            } catch (IOException ex1) {
            }
            result = false;
        }

        return result;
    }

    public boolean isBlackFirst() {
        return blackFirst;
    }
    
    public void setBlackFirst(boolean blackFirst)
    {
        this.blackFirst = blackFirst;
    }
            
}
