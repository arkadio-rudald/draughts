/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcaby;

import java.io.Serializable;

/**
 *
 * @author Arkadio
 */
public class Move implements Serializable {
    private int idMove;
    private boolean black;
    private boolean king;
    private int idChecker;
    private int idField;

    public Move(int idMove, boolean black, boolean king, int idChecker, int idField)
    {
        this.idMove = idMove;
        this.black = black;
        this.king = king;
        this.idChecker = idChecker;
        this.idField = idField;        
    }
    
    public int getIdMove() {
        return idMove;
    }

    public boolean isBlack() {
        return black;
    }

    public boolean isKing() {
        return king;
    }

    public int getIdChecker() {
        return idChecker;
    }

    public int getIdField() {
        return idField;
    }

    public void setIdField(int idField) {
        this.idField = idField;
    }
    
    
}
