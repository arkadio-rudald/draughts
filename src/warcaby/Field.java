/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcaby;

import java.io.Serializable;

/**
 * Pole składa się z dwóch punktów, położonych przeciwlegle, które określają jego powierzchnię
 * pole id jednoznacznie określa dane Pole
 * pole "wolne" oznacza czy pole nie jest zajęte przez pionek
 * @author Arkadio
 */
public class Field implements Serializable{
    private boolean free;
    private boolean forbidden;
    
    private int x1;
    private int y1;
    private int x2;
    private int y2;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getX1() {
        return x1;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public int getY1() {
        return y1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getY2() {
        return y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }
    
    public int getIdByCoordinates(int xk1, int xk2, int yk1, int yk2)
    {
        if(xk1 == x1 && yk1 == y1 && xk2 == x2 && yk2 == y2)
        {
            return this.id;
        }
        else
        {
            return -1;
        }
    }

    public void setForbidden(boolean prohibited) {
        this.forbidden = prohibited;
    }

    public boolean isForbidden() {
        return forbidden;
    }
    
    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }
}
