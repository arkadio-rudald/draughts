/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcaby;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;
import warcaby.NetworkConnection.ErrorHandling;
import warcaby.NetworkConnection.Player;
import warcaby.NetworkConnection.Server;
import warcaby.NetworkConnection.ServerScannerBroadcast;


/**
 *
 * @author Arkadio
 */
public class Checkers extends JFrame implements ActionListener{
    public static int fieldSize = 75;
    public static final int boardSize = 8;
    private ControlGame control;
    public OutputLogsPanel logs;
    
    //Internet
    private Server server;        
    public static int NUMBER_OF_SCANNING_THREADS = 254;

    Player player1, player2;
    
    Checkers()
    {
        Dimension applicationSize = customizeDimension();
        setSize(applicationSize.width,applicationSize.height);        
        setResizable(false);

        
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        
        JMenu fileMenu = new JMenu("Actions");
        menuBar.add(fileMenu);
        
        final JMenuItem newGameAction = new JMenuItem("New Game");
        final JMenuItem saveGameAction = new JMenuItem("Save Game");
        final JMenuItem loadGame = new JMenuItem("Load Game");
        final JMenuItem loadAction = new JMenuItem("Load game to check");
        final JMenuItem closeAction = new JMenuItem("Close game");
        
        logs = new OutputLogsPanel();
        
        fileMenu.add(newGameAction);
        fileMenu.add(saveGameAction);
        fileMenu.add(loadGame);
        fileMenu.add(loadAction);
        fileMenu.add(closeAction);
        saveGameAction.setEnabled(false);
        closeAction.setEnabled(false);

        JMenu networkMenu = new JMenu("Network");
        menuBar.add(networkMenu);
        
        final JMenuItem hostServer = new JMenuItem("Create Server");
        final JMenuItem joinGame = new JMenuItem("Join game");
        final JMenuItem closeConnection = new JMenuItem("Close connection");
        
        networkMenu.add(hostServer);
        networkMenu.add(joinGame);
        networkMenu.add(closeConnection);
        closeConnection.setEnabled(false);              
        
        hostServer.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                //joinGame.setEnabled(false);
                newGameAction.setEnabled(false);
                loadGame.setEnabled(false);
                saveGameAction.setEnabled(false);
                loadAction.setEnabled(false);
                closeAction.setEnabled(false);
                hostServer.setEnabled(false);
                closeConnection.setEnabled(true);
                
                server = new Server();
                System.out.println("server was started");
                
                
                try 
                {
                    JPanel bigPanel = new JPanel();
                    bigPanel.setLayout(null);

                    player1 = new Player(1,boardSize, fieldSize);


                    player1.gameControl.setBounds(10, 10, boardSize*fieldSize , boardSize*fieldSize);
                    //player1.gameControl.setSize(boardSize*fieldSize, boardSize*fieldSize);
                    player1.gameControl.addMouseListener(player1.gameControl);
                    player1.gameControl.addMouseMotionListener(player1.gameControl);
                    player1.gameControl.setFocusable(true);

                    player1.gameControl.setPreferredSize(new Dimension(boardSize*fieldSize,boardSize*fieldSize));
                    player1.gameControl.setLayout(null);

                    player1.connectWithServer(server.serverSocket.getInetAddress().getHostAddress());

                    Dimension gameDimension = player1.gameControl.getPreferredSize();

                    JButton whiteButton = new JButton("White");
                    JButton blackButton = new JButton("Black");
                    
                    Dimension dimButton = blackButton.getPreferredSize();
                    whiteButton.setBounds(gameDimension.width + 20, 10, dimButton.width + 20 , dimButton.height);
                    blackButton.setBounds(gameDimension.width + 20, 20 + dimButton.height, dimButton.width + 20 , dimButton.height);
                    
                    whiteButton.setPreferredSize(new Dimension(dimButton.width + 20, dimButton.height));
                    blackButton.setPreferredSize(new Dimension(dimButton.width + 20, dimButton.height));
                    
                    bigPanel.add(whiteButton);
                    bigPanel.add(blackButton);

                    bigPanel.add(player1.gameControl);
                    bigPanel.setPreferredSize(new Dimension(gameDimension.width + whiteButton.getWidth() + 50, getJMenuBar().getHeight() +  gameDimension.height+50));
                    bigPanel.setSize(new Dimension(gameDimension.width + whiteButton.getWidth(), gameDimension.height));
                    add(bigPanel);
                    
                    setSize(player1.gameControl.getWidth() + whiteButton.getWidth() + 50,getJMenuBar().getHeight() + player1.gameControl.getHeight() + 50);
                    setPreferredSize(new Dimension(player1.gameControl.getWidth() + whiteButton.getWidth() + 20,getJMenuBar().getHeight() + player1.gameControl.getHeight() + 30));
                    
                    Dimension frameDimension = getSize();
                    setPreferredSize(frameDimension);
                    setSize(frameDimension);
                    
                    pack();
                    
                    player1.gameControl.isBlock = true;
                    
                    blackButton.setEnabled(false);
                    whiteButton.setEnabled(false);
                    
                    player1.start();
                    whiteButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            blackButton.setEnabled(false);
                            whiteButton.setEnabled(false);
                            
                        }
                    });
                    
                    blackButton.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {    
                            whiteButton.setEnabled(false);
                            blackButton.setEnabled(false);
                        }
                    });
                            
                    
                }
                catch(IOException ex)
                {
                    try {
                        ErrorHandling errorCall = new ErrorHandling();
                        errorCall.sendPOST(errorCall.getErrorDetails(ex));
                    } catch (IOException ex1) {
                    }
                }
            }
        });
        
        joinGame.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                hostServer.setEnabled(false);
                //joinGame.setEnabled(false);
                newGameAction.setEnabled(false);
                loadGame.setEnabled(false);
                saveGameAction.setEnabled(false);
                loadAction.setEnabled(false);
                closeAction.setEnabled(false);
                closeConnection.setEnabled(true);
                ServerScannerBroadcast ssb;
                
                JPanel mainPanel = (JPanel) getContentPane();
                try {
                    joinGame.setEnabled(false);
                    ssb = new ServerScannerBroadcast(mainPanel);
                    Thread ssbt = new Thread(ssb);
                    ssbt.start();                    
                    
                } catch (SocketException ex) {
                    Logger.getLogger(Checkers.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        });
        
        closeConnection.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
         
                try 
                {
                    if(server != null)
                    {
                        if(server.getPlayerBlackSocket() != null)
                        {   
                            server.sendMessage(server.getPlayerBlackSocket(), "closeConnection");
                            server.getPlayerBlackSocket().close();
                        }
                        if(server.getPlayerWhiteSocket() != null)
                        {
                            server.sendMessage(server.getPlayerWhiteSocket(), "closeConnection");
                            server.getPlayerWhiteSocket().close();
                        }                        
                        if(server.getServerSocket() != null)
                        {
                            server.getServerSocket().close();
                        }
                    }
                    

                    //a teraz wyczyszczenie ekranu
                } 
                catch (IOException ex) 
                {
                    Logger.getLogger(Checkers.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                if(player1 !=null)
                {
                    player1.isOpenedConnection = false;
                }
                JPanel pane = (JPanel) getContentPane();
                pane.removeAll();
                remove(pane);
                
                JPanel panel = new JPanel();
                setContentPane(panel);
                repaint();
                
                closeConnection.setEnabled(false);
                hostServer.setEnabled(true);
                joinGame.setEnabled(true);
                newGameAction.setEnabled(true);
                loadGame.setEnabled(true);
                saveGameAction.setEnabled(true);
                loadAction.setEnabled(true);
                closeAction.setEnabled(true);
            }
        });
        

        
        newGameAction.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                saveGameAction.setEnabled(true);
                loadGame.setEnabled(false);
                closeAction.setEnabled(true);
                                
                //sterowanie = new Sterowanie();
                
                if(control == null)
                {
                    control = new ControlGame(boardSize, fieldSize);
                    control.setBounds(10, getJMenuBar().getHeight() + 10, boardSize*fieldSize + 10, boardSize*fieldSize + 10);

                    control.setSize(boardSize*fieldSize, boardSize*fieldSize);
                    
                    control.addMouseListener(control);
                    control.addMouseMotionListener(control);
                    control.setFocusable(true);
                    
                    add(control);
                    setContentPane(control);
                    repaint();
                }
                else
                {
                    ControlGame temp = new ControlGame(boardSize, fieldSize);

                    control.checkerboard = temp.checkerboard;
                    control.rules = new Principle(control.checkerboard.getBoard(), boardSize, fieldSize);
                    control.historyGame = new HistoryMovements(boardSize, fieldSize);
                    control.historyGame.getCheckerList().add(control.historyGame.saveInCheckerLists(control.rules.getWhites(), control.rules.getBlacks()));
                    control.setBlackFirst(false);
                    
                    setContentPane(control);
                    temp = null;
                    repaint();
                }
            }
        
        });
        
        //Zapis ostatnich ruchów i obiektów
        saveGameAction.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
           
                try {
                    boolean suc =  new File("unfinishedGames/").mkdir();
                    if(new File("unfinishedGames/").isDirectory())
                    {
                        control.historyGame.saveMovementsToFile(control.historyGame.findMovesInMovementsLists(),false);
                        control.historyGame.saveGameObjects(control.rules.getWhites(), control.rules.getBlacks(), control.isBlackFirst());
                    }
                    //control.historyGame.saveMovementsToFile(control.historyGame.findMovesInMovementsLists(), false);
                    
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Checkers.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        //Załadowanie obiektów
        loadGame.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {                                
            
                final JFileChooser fc = new JFileChooser();
                closeAction.setEnabled(true);
                
                FileNameExtensionFilter filter = new FileNameExtensionFilter("TEXT FILES", "txt", "text");
                
                fc.setFileFilter(filter);
                fc.changeToParentDirectory();
                fc.showOpenDialog(fc);
                File file = fc.getSelectedFile();
                try {
                    if(control == null)
                    {
                        control = new ControlGame(boardSize, fieldSize);
                            control.setBounds(10, getJMenuBar().getHeight() + 10, boardSize*fieldSize + 10, boardSize*fieldSize + 10);

                            control.setSize(boardSize*fieldSize, boardSize*fieldSize);

                            control.addMouseListener(control);
                            control.addMouseMotionListener(control);
                            control.setFocusable(true);
                            add(control);
                            setContentPane(control);
                            repaint();
                    }
                    else
                    {
                            ControlGame temp = new ControlGame(boardSize, fieldSize);

                            control.checkerboard = temp.checkerboard;
                            control.rules = new Principle(control.checkerboard.getBoard(), boardSize, fieldSize);
                            control.historyGame = new HistoryMovements(boardSize, fieldSize);
                            control.historyGame.getCheckerList().add(control.historyGame.saveInCheckerLists(control.rules.getWhites(), control.rules.getBlacks()));
                            control.setBlackFirst(false);

                            setContentPane(control);
                            temp = null;
                            repaint();
                    }
                    if(control.isFileCorrect(file))
                        control.loadGame(file);
                    else
                    {
                        //SHOW DIALOG WRONG FILE
                    }
                } 
                catch (FileNotFoundException | NullPointerException ex){
                    //Logger.getLogger(Checkers.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        });
        
        loadAction.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                control.historyGame.createHistoryFrame();
            }
        });             
        
        closeAction.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                saveGameAction.setEnabled(false);
                loadGame.setEnabled(true);
                
                JPanel jpanel = new JPanel();
                setContentPane(jpanel);
                repaint();
                control.removeAll();                
                closeAction.setEnabled(false);
            }
        });
        
        
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    
    public void actionPerformed(ActionEvent e) 
    {
        control.requestFocus();        
        repaint();
    }
    
    public Dimension customizeDimension()
    {
        Toolkit tools = Toolkit.getDefaultToolkit();
        Dimension screenSize = tools.getScreenSize();
    
        //rozmiar aplikacji dostosować do rozdzielczości,
        //rozmiar pola gry dostosować do okna aplikacji
        //historie dostosować do aplikacji
        Dimension applicationSize = new Dimension();
        
        int fieldSize = ((applicationSize.height-10)*90) /(boardSize*100) ;
        
        if(screenSize.height > screenSize.width)
        {
            applicationSize.width = (screenSize.width/100)*80;
            applicationSize.height = (screenSize.width/100)*80 + 30;
            fieldSize = (applicationSize.width-20) /boardSize ;
        }
        else
        {
            applicationSize.width = (screenSize.height/100)*80;
            applicationSize.height = (screenSize.height/100)*80 + 50;                        
            fieldSize = (((screenSize.height/100)*80 - 20)*100) /(boardSize*100);
        }
        
        //to remove
        setFieldSize(fieldSize);
        
        return applicationSize;
    }
    
    
    public void setFieldSize(int fieldSize) {        
        this.fieldSize = fieldSize;
    }                
}
