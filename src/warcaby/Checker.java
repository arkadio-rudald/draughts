/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcaby;

import java.awt.Color;
import java.awt.Graphics;
import java.io.Serializable;

/**
 *
 * @author Arkadio
 */
public class Checker implements Serializable{
    private boolean king;
    private Field field;
    private int id;
    private boolean black;
    private int toDrawX;
    private int toDrawY;
    
    public Checker()
    {
        field = new Field();
    }
    
    public Checker(Field getField)
    {
        field = getField;
        toDrawX = field.getX1();
        toDrawY = field.getY1();        
    }

    public boolean isBlack() {
        return black;
    }

    public void setBlack(boolean black) {
        this.black = black;
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;        
        toDrawX = this.field.getX1();
        toDrawY = this.field.getY1();
    }

    public boolean isKing() {
        return king;
    }
    
    public int getToDrawX() {
        return toDrawX;
    }

    public void setToDrawX(int toDrawX) {
        this.toDrawX = toDrawX;
    }
    
    public int getToDrawY() {
        return toDrawY;
    }

    public void setToDrawY(int toDrawY) {
        this.toDrawY = toDrawY;
    }
    

    public void setKing(boolean king) {
        this.king = king;
    }

    public void drawChecker(Graphics g, int checkerSize)
    {
        int d = 8*checkerSize/10;    
        int difference = (checkerSize - 8*checkerSize/10);
        
        g.fillOval(toDrawX + 2 + difference/2, toDrawY + 2 + difference/2, 8 * checkerSize / 10, 8*checkerSize/10);      
        
        if(isKing())
        {
            g.setColor(Color.red);
            g.drawLine(toDrawX + difference/2 + d/6 + 2, toDrawY + difference/2 + 2*d/3 + 2, toDrawX + difference/2 + 5*d/6 + 2, toDrawY + difference/2 + 2*d/3 + 2);
            g.drawLine(toDrawX + difference/2 + d/6 + 2, toDrawY + difference/2 + 2*d/3 + 2, toDrawX + difference/2 + d/6 + 2, toDrawY + difference/2 + d/3 + 2);
            g.drawLine(toDrawX + difference/2 + 5*d/6 + 2, toDrawY + difference/2 + 2*d/3 + 2, toDrawX + difference/2 + 5*d/6 + 2, toDrawY + difference/2 + d/3 + 2);
            g.drawLine(toDrawX + difference/2 + d/6 + 2, toDrawY + difference/2 + d/3 + 2, toDrawX + difference/2 + 2*d/7 + 2, toDrawY + difference/2 + 2*d/3 + 2);
            g.drawLine(toDrawX + difference/2 + 2*d/6 + 2, toDrawY + difference/2 + 2*d/3 + 2, toDrawX + difference/2 + 3*d/6 + 2, toDrawY + difference/2 + d/3 + 2);
            g.drawLine(toDrawX + difference/2 + 3*d/6 + 2, toDrawY + difference/2 + d/3 + 2, toDrawX + difference/2 + 4*d/6 + 2, toDrawY + difference/2 + 2*d/3 + 2);
            g.drawLine(toDrawX + difference/2 + 4*d/6 + 2, toDrawY + difference/2 + 2*d/3 + 2, toDrawX + difference/2 + 5*d/6 + 2, toDrawY + difference/2 + d/3 + 2);
            
            int [] xPoints = new int [7];
            xPoints[0] = toDrawX + difference/2 + d/6 + 2;
            xPoints[1] = toDrawX + difference/2 + d/6 + 2;
            xPoints[2] = toDrawX + difference/2 + 2*d/6 + 2;
            xPoints[3] = toDrawX + difference/2 + 3*d/6 + 2;
            xPoints[4] = toDrawX + difference/2 + 4*d/6 + 2;
            xPoints[5] = toDrawX + difference/2 + 5*d/6 + 2;
            xPoints[6] = toDrawX + difference/2 + 5*d/6 + 2;
            
            int [] yPoints = new int [7];
            yPoints[0] = toDrawY + difference/2 + 2*d/3 + 2;
            yPoints[1] = toDrawY + difference/2 + d/3 + 2;
            yPoints[2] = toDrawY + difference/2 + 2*d/3 + 2;
            yPoints[3] = toDrawY + difference/2 + d/3 + 2;
            yPoints[4] = toDrawY + difference/2 + 2*d/3 + 2;
            yPoints[5] = toDrawY + difference/2 + d/3 + 2;
            yPoints[6] = toDrawY + difference/2 + 2*d/3 + 2;
         
            g.fillPolygon(xPoints, yPoints, 7);        
        }
        g.setColor(Color.BLACK);
        g.drawString(String.valueOf(id), toDrawX + checkerSize/2, toDrawY + checkerSize/2);
        
    }    

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Checker other = (Checker) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.black != other.black) {
            return false;
        }
        return true;
    }
    
    public boolean equalPosition(Checker checker)
    {
        if(checker.getField().getId() == this.getField().getId())
            return true;  
        
        return false;
    }      
}
