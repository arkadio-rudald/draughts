/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcaby;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Arkadio
 */
public class Principle 
{
    private List <Checker> blacks; 
    private List <Checker> whites; 
    private Checker markedChecker;
    
    private final List <Field> checkerboard;
    
    private int boardSize;
    private int fieldSize;
    public boolean netGame = false;

    public Principle(List <Field> board, int boardSize, int fieldSize)
    {
        this.checkerboard = board;
        this.boardSize = boardSize;
        this.fieldSize = fieldSize;
        createCheckers();
    }
    
    public int getBoardSize() {
        return boardSize;
    }

    public void setBoardSize(int boardSize) {
        this.boardSize = boardSize;
    }

    public int getFieldSize() {
        return fieldSize;
    }

    public void setFieldSize(int fieldSize) {
        this.fieldSize = fieldSize;
    }
    
    public List<Checker> getBlacks() {
        return blacks;
    }

    public void setBlacks(List<Checker> blacks) {
        this.blacks = blacks;
    }

    public List<Checker> getWhites() {
        return whites;
    }

    public void setWhites(List<Checker> whites) {
        this.whites = whites;
    }

    public Checker getMarkedChecker() {
        return markedChecker;
    }

    public void setMarkedChecker(Checker markPionek) {
        this.markedChecker = markPionek;
    }
    
    public Field getFieldById(int idField)
    {
        for(Field field: checkerboard)
        {
            if(idField == field.getId())
            {
                return field;
            }
        }
        return null;
    }
    
    public Checker getCheckerByIdAndColour(int id, boolean isBlack)
    {
        if(isBlack)
        {
            for(Checker checkerBlack: blacks) 
                if(checkerBlack.getId() == id)
                    return checkerBlack;            
        }
        else
        {
            for(Checker checkerWhite: whites) 
                if(checkerWhite.getId() == id)
                    return checkerWhite;            
        }
                    
        return null;
    }
    
    
    public void createCheckers()
    {
        blacks = new ArrayList();
        whites = new ArrayList();
        
        int x1,y1,x2,y2; //współrzędne rysowania 2 okręgów x1,y1 - czarnego i x2 y2 - białego
        
        for(int i = 0; i < (boardSize/2)-1; i++)      
        {
            y1 = fieldSize * i;
            y2 = (boardSize*fieldSize - 1) - i*fieldSize;
            
            for(int j = 0; j < boardSize; j = j + 2)
            {
                if(i % 2 == 1)
                {
                    x1 = fieldSize * j;
                    x2 = fieldSize * j + fieldSize;
                }
                else
                {
                    x1 = fieldSize*j + fieldSize;
                    x2 = fieldSize*j;
                }
                
                Checker checker = new Checker(checkField(x1,y1));
                checker.getField().setFree(false);
                checker.setId((i*boardSize/2)+j/2);
                checker.setBlack(true);
                blacks.add(checker);
                
                checker = new Checker(checkField(x2,y2));
                checker.getField().setFree(false);
                checker.setId((i*boardSize/2)+j/2);
                checker.setBlack(false);
                whites.add(checker);                
            }
        }
    }
    
    public Checker isCheckerToBeAKing(Checker checker, boolean wasBattle)
    {        
        if(netGame)
        {
            if((checker.getField().getY1() == 0 && !wasBattle) ||
                checker.getField().getY1() == 0 && !isBattle(checker)
                || checker.getField().getY2() == (fieldSize*boardSize)-1 
                        && !isBattle(checker))
                
                checker.setKing(true);     
        }
        else
        {
            if(checker.isBlack())
            {
                if(checker.getField().getY2() == (fieldSize*boardSize)-1 
                        && !isBattle(checker))
                {
                    checker.setKing(true);                    
                }
            }
            else
            {
                //if(checker.getField().getY1() == 0 && !isBattle(checker))
                if((checker.getField().getY1() == 0 && !wasBattle) ||
                    checker.getField().getY1() == 0 && !isBattle(checker))

                    checker.setKing(true);                
            }
        }
        return checker;
    }
    
    public void removeTakenChecker(Checker checker, Field currentField, Field lastField)
    {
        if(checker.isKing())
        {
            int numberOfFieldsToLook = Math.abs(currentField.getX1() - lastField.getX1())/fieldSize;
            
            if(lastField.getX1() - currentField.getX1() > 0)
            {
                if(lastField.getY1() - currentField.getY1() > 0)
                {
                    for(int i = 1; i < numberOfFieldsToLook; i++)
                    {
                        Field fieldToClear = checkField(lastField.getX1() - i*fieldSize, lastField.getY1() - i*fieldSize);
                        Checker killedChecker = isCheckerOnBattleField(fieldToClear);
                        if(killedChecker != null)
                        {
                            removeChecker(killedChecker);
                            fieldToClear.setForbidden(false);
                            fieldToClear.setFree(true);                
                        }
                    }
                }
                else
                {
                    for(int i = 0; i < numberOfFieldsToLook; i++)
                    {
                        Field fieldToClear = checkField(lastField.getX1() - i*fieldSize, lastField.getY1() + i*fieldSize);
                        Checker killedChecker = isCheckerOnBattleField(fieldToClear);
                        if(killedChecker != null)
                        {
                            removeChecker(killedChecker);
                            fieldToClear.setForbidden(false);
                            fieldToClear.setFree(true);                
                        }
                    }
                }
            }
            else
            {
                if(lastField.getY1() - currentField.getY1() > 0)
                {
                    for(int i = 0; i < numberOfFieldsToLook; i++)
                    {
                        Field fieldToClear = checkField(lastField.getX1() + i*fieldSize, lastField.getY1() - i*fieldSize);
                        Checker killedChecker = isCheckerOnBattleField(fieldToClear);
                        if(killedChecker != null)
                        {
                            removeChecker(killedChecker);
                            fieldToClear.setForbidden(false);
                            fieldToClear.setFree(true);                
                        }
                    }
                }
                else
                {
                    for(int i = 0; i < numberOfFieldsToLook; i++)
                    {
                        Field fieldToClear = checkField(lastField.getX1() + i*fieldSize, lastField.getY1() + i*fieldSize);
                        Checker killedChecker = isCheckerOnBattleField(fieldToClear);
                        if(killedChecker != null)
                        {
                            removeChecker(killedChecker);
                            fieldToClear.setForbidden(false);
                            fieldToClear.setFree(true);                
                        }
                    }
                }
            }
        }
        else
        {
            Field fieldToClear = checkField((currentField.getX1() + lastField.getX1())/2, (currentField.getY1() + lastField.getY1())/2);
            Checker killedChecker = isCheckerOnBattleField(fieldToClear);
            removeChecker(killedChecker);
            fieldToClear.setForbidden(false);
            fieldToClear.setFree(true);                
        }
    }
       
    public void removeChecker(Checker checker)
    {
        if(checker.isBlack())
        {
            blacks.remove(checker);
        }
        else
        {
            whites.remove(checker);
        }
    }
    
    public Checker isEnemyOnField(Field field, Checker checker)
    {
        if(checker.isBlack())
        {
            for(Checker white: whites)
            {
                if(field.getId() == white.getField().getId())
                {
                    return white;
                }
            }
        }
        else
        {
            for(Checker black: blacks)
            {
                if(field.getId() == black.getField().getId())
                {
                    return black;
                }           
            }
        }
        return null;
    }
    
    public Checker isAmigoOnField(Field field, Checker checker)
    {
        if(!checker.isBlack())
        {
            for(Checker white: whites)
            {
                if(field.getId() == white.getField().getId())
                {
                    return white;
                }
            }
        }
        else
        {
            for(Checker black: blacks)
            {
                if(field.getId() == black.getField().getId())
                {
                    return black;
                }           
            }
        }
        return null;
    }
    
    public List<Checker> isBattle(boolean black)
    {
        List<Checker> result = new ArrayList<Checker>();
        if(black)
        {
            for(Checker checker: blacks)
            {
                if(isBattle(checker))
                {
                    result.add(checker);
                }
            }
        }
        else
        {
            for(Checker checker: whites)
            {
                if(isBattle(checker))
                {
                    result.add(checker);
                    System.out.println(checker.getId() + " " + checker.getField().getId());
                }
            }
        }       
        return result;
    }
    
    public boolean isBattle(Checker checker)
    {
        boolean result = false;
        boolean checkNextFields = false;
        boolean breakLoop = false;
        //sprawdzamy czy damka może bić
        
        if(checker.isKing())
        {
            for(int i = -fieldSize + checker.getField().getX1();
                    i < fieldSize + 1 + checker.getField().getX1(); i = i + 2*fieldSize)
            {
                for(int j = -fieldSize + checker.getField().getY1();
                            j < fieldSize + 1 + checker.getField().getY1(); j = j + 2*fieldSize)
                {
                    int positionX = i;
                    int positionY = j;
                    int iterator = 1;
                    while(positionX > 0 && positionX + fieldSize < boardSize*fieldSize &&
                            positionY > 0 && positionY + fieldSize < boardSize*fieldSize )
                    {
                        positionX = checker.getField().getX1() + ((i - checker.getField().getX1()) * iterator);
                        positionY = checker.getField().getY1() + ((j - checker.getField().getY1()) * iterator);
                        iterator++;    
                                                
                        if(checkNextFields)
                        {
                            Field fieldAfterKilledChecker = checkField(positionX, positionY);
                            if(fieldAfterKilledChecker != null)
                            {
                                if(fieldAfterKilledChecker.isFree())
                                {
                                    if(checker.getField().getId() == markedChecker.getField().getId())
                                    {
                                        fieldAfterKilledChecker.setForbidden(false);
                                    }
                                    result = true;
                                }
                                else
                                {
                                    checkNextFields = false;
                                    breakLoop = true;
                                }
                            }
                        }             
                        if(breakLoop)
                        {
                            breakLoop = false;
                            break;
                        }
                        if(getCheckerFromField(checkField(positionX,positionY)) != null || isCheckerOnBattleField (checkField(positionX,positionY)) != null)
                        {
                            if(isAmigoOnField(checkField(positionX,positionY), checker) != null)
                            {
                                //jeśli nie jest to pole skrajne
                                if(positionX != 0 && positionY != 0 && positionX != fieldSize*(boardSize-1) && positionY != fieldSize*(boardSize-1))
                                {
                                    breakLoop = true;
                                }
                            }
                            if(isEnemyOnField (checkField(positionX,positionY), checker) != null && !isExtremeField(checkField(positionX,positionY)))
                            {
                                checkNextFields = true;                            
                            }
                        }
                    }                
                }
            }
        }
        else
        {
            for(int i = -fieldSize + checker.getField().getX1();
                    i < fieldSize + 1 + checker.getField().getX1(); i = i + fieldSize)
            {
                for(int j = -fieldSize + checker.getField().getY1();
                        j < fieldSize + 1 + checker.getField().getY1(); j = j + fieldSize)
                {
                    if( i > 0 && i < boardSize*fieldSize &&
                            j > 0 && j < boardSize*fieldSize) 
                    {
                        if(isEnemyOnField (checkField(i,j), checker) != null)
                        {
                            Field fieldAfterKilledChecker = checkField(2 * i - checker.getField().getX1(),2 * j - checker.getField().getY1());
                            if(fieldAfterKilledChecker != null)
                            {
                                if(fieldAfterKilledChecker.isFree())
                                {
                                    if(checker.getField().getId() == markedChecker.getField().getId())
                                    {
                                        fieldAfterKilledChecker.setForbidden(false);
                                    }
                                    result = true;
                                }                            
                            }
                        }
                    }
                }
            }
        }
        return result;
    }
    
    public boolean isExtremeField(Field field)
    {
        if(field.getX1() == 0 || field.getX2() == boardSize*fieldSize-1
                || field.getY1() == 0 || field.getY2() == boardSize*fieldSize-1)
        {
            return true;
        }
        return false;
    }
    
    public Checker isCheckerOnBattleField(Field field)
    {
        if(!field.isFree())
        {
            for(Checker black: blacks)
            {
                if(black.getField().getId() == field.getId())
                {
                    return black;
                }
            }
            for(Checker white: whites)
            {
                if(white.getField().getId() == field.getId())
                {
                    return white;
                }
            }
        }
        return null;
    }
    
    public boolean isCorrectField(Field field, Checker markedChecker)
    {
        if(!field.isForbidden() && field.isFree() && markedChecker != null)
        {
            markedChecker.getField().setFree(true);
            //System.out.println("Pole Pionek X1 " + marksPionek.getPole().getX1());
            markedChecker.setField(field);            
            //System.out.println("Pole X1 " + marksPionek.getPole().getX1());                  
            return true;
        }        
        return false;
    }
    
    public void movingChecker(Checker checker, Field lastField, Field currentField)
    {
        lastField.setFree(true);
        currentField.setFree(false);        
        checker.setField(currentField);
    }
    
    public Checker getCheckerFromField(Field field)
    {
        if(!field.isForbidden() && !field.isFree())
        {
            for(Checker black: blacks)
            {
                if(black.getField().getId() == field.getId())
                {
                    return black;
                }
            }
            for(Checker white: whites)
            {
                if(white.getField().getId() == field.getId())
                {                    
                    return white;
                }
            }
        }
        return null;
    }
    
    
    
    public Field checkField(int positionX, int positionY) //Podajemy punkt i zwracamy pole do którego należy
    {
        Field field = new Field();
        for(int i = 0; i < checkerboard.size(); i++)
        {
            if(positionX >= checkerboard.get(i).getX1() && positionY >= checkerboard.get(i).getY1()
                    && positionX <= checkerboard.get(i).getX2() && positionY <= checkerboard.get(i).getY2())
            {
                field = checkerboard.get(i);
                return field;
            }
        }
        return null;
    }
    
    public boolean isCheckerOnField(Field field)
    {
        for(Checker black: blacks)
        {
            if(black.getField().getId() == field.getId())
            {
                return true;
            }
        }
        for(Checker white: whites)
        {
            if(white.getField().getId() == field.getId())
            {                    
                return true;
            }
        }
        return false;
    }
    
    //DO USUNIĘCIA - SPRAWDZIĆ
    public boolean isForbiddenField(Checker checker, Field field)
    {
        if(!checker.isBlack() &&
                Math.abs(field.getX1() - checker.getField().getX1()) <= fieldSize 
                && (field.getY1() - checker.getField().getY1() > 0 && 
                    field.getY1() - checker.getField().getY1() <= fieldSize))
        {
            return true;
        }
        else if(checker.isBlack() &&
                Math.abs(field.getX1() - checker.getField().getX1()) >= fieldSize 
                && (checker.getField().getY1() - field.getY1() > 0 && 
                    checker.getField().getY1() - field.getY1() <= fieldSize))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    //Sprawdza czy Pole jest oddalone o co najmniej 2 Pola względem Pionka
    public boolean isFieldFurtherThan1Field(Checker checker, Field field)
    {
        if(Math.abs(field.getX1() - checker.getField().getX1()) > fieldSize || 
           Math.abs(field.getY1() - checker.getField().getY1()) > fieldSize)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public boolean isFieldGoingForward(Checker checker, Field field)
    {
        if(netGame)
        {
            if(checker.getField().getY1() - field.getY1() > 0)
                return true;
            else
                return false;                
        }
        else
        {
            if(!checker.isBlack() && checker.getField().getY1() - field.getY1() > 0)
            {
                return true;
            }
            else if(checker.isBlack() && field.getY1() - checker.getField().getY1() > 0 )
            {
                return true;
            }
            else
            {
                return false;
            }
        }        
    }

    
    public void activateFieldsForKing(Checker checker)
    {
        for(int i = -fieldSize + checker.getField().getX1();
                    i < fieldSize + 1 + checker.getField().getX1(); i = i + 2*fieldSize)
        {
                for(int j = -fieldSize + checker.getField().getY1();
                            j < fieldSize + 1 + checker.getField().getY1(); j = j + 2*fieldSize)
                {
                    int positionX = i;
                    int positionY = j;
                    int iterator = 1;
                    while(positionX >= 0 && positionX + fieldSize <= boardSize*fieldSize &&
                            positionY >= 0 && positionY + fieldSize <= boardSize*fieldSize )
                    {
                        //positionX = checker.getField().getX1() + ((i - checker.getField().getX1()) * iterator);
                        //positionY = checker.getField().getY1() + ((j - checker.getField().getY1()) * iterator);
                        iterator++;                    
                        if(isCheckerOnField(checkField(positionX,positionY)))
                        {
                            break;
                        }
                        else
                        {
                            checkField(positionX,positionY).setForbidden(false);
                        }
                        positionX = checker.getField().getX1() + ((i - checker.getField().getX1()) * iterator);
                        positionY = checker.getField().getY1() + ((j - checker.getField().getY1()) * iterator);
                    }                
                }
        }
    }
    
    //Blokuje wolne pola na których pionek nie może stanąć
    public void blockadeImpossiblyFieldToPutChecker(Checker checker)
    {
        //System.out.println("Pole pionka " + pionek.getPole().getX1() + " " + pionek.getPole().getY1());       
            if(checker.isKing())
            {
                for (Field field : checkerboard) 
                {
                    if((isFieldFurtherThan1Field(checker, field)) && field.isFree())
                        field.setForbidden(true);                                    
                }
                
                blockadeFreeFields();
                activateFieldsForKing(checker);
            }
            else
            {
                for (Field field : checkerboard) 
                {
                    if(isFieldGoingForward(checker, field) && !isFieldFurtherThan1Field(checker, field))
                        field.setForbidden(false);                                    
                    else
                        field.setForbidden(true);                    
                }
            }                   
    }
    
    public void blockadeFreeFields()
    {
        for (Field field : checkerboard) 
        {
            if(field.isFree())
            {
                field.setForbidden(true);
            }                    
        }     
    }
    
    /** 
     * 
     */
    
    public void activateAllFreePossiblyFields()
    {
        for (Field field : checkerboard) 
        {
            if(field.isFree())
            {
                field.setForbidden(false);
            }                    
        }     
    }
    
    public void changeFieldsToFreeIfEmpty()
    {
        for (Field field : checkerboard) 
        {
            if(!isCheckerOnField(field) && !field.isForbidden())
                field.setFree(true);
            else
                field.setFree(false);
        }     
    }
       
    
    public void blockadeCheckers(List<Checker> checkerList)
    {
        for (Checker checker : checkerList) 
        {
            checker.getField().setForbidden(true);
        }            
    }
    public void activateCheckers(List<Checker> checkerList)
    {
        for (Checker checker : checkerList) 
        {
            checker.getField().setForbidden(false);
        }
    }
    
}
