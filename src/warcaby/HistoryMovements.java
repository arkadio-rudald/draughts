/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcaby;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;
import warcaby.NetworkConnection.ErrorHandling;


/**
 *
 * Rest always helps - if you can't get the solution try to easy tension.
 * @author Arkadio
 */
public class HistoryMovements extends JFrame implements ActionListener{
    private ArrayList <List <Checker>> checkersMovementsList = new ArrayList<List <Checker>>();
    private List <Checker> allCheckerList;
    private List <String> movementsToSave;
    
    HistoryMovementsPanel historyPanel;
    JButton bChoosenFile = new JButton("Wybierz rozgrywke");
    JButton bPreviousMovement = new JButton("<");
    JButton bNextMovement = new JButton(">");
    private int boardSize = 0, fieldSize = 0;
    
    JFileChooser fc;
    
    private List <Move> forwardMovement, backMovement = new ArrayList<>();
    private int movementNumber = 1;
    
    public ArrayList<List<Checker>> getCheckerList() {
        return checkersMovementsList;
    }

    public void setCheckerList(ArrayList<List<Checker>> ArrayList) {
        this.checkersMovementsList = ArrayList;
    } 

    public void setForwardMovement(List<Move> forwardMovement) {
        this.forwardMovement = forwardMovement;
    }

    public void setBackMovement(List<Move> backMovement) {
        this.backMovement = backMovement;
    }

    public void setMovementsToSave(List<String> movementsToSave) {
        this.movementsToSave = movementsToSave;
    }
    
    
    
    
    public HistoryMovements(int boardSize, int fieldSize)
    {
        this.boardSize = boardSize;
        this.fieldSize = fieldSize;                            
    }
    
    //save the movement of checker
    public List<Checker> saveInCheckerLists(List <Checker> whiteList, List <Checker> blackList)
    {
        allCheckerList = new ArrayList <Checker>();
        
        for(Checker whiteChecker: whiteList)
        {
            Checker checker = new Checker();
            checker.setBlack(whiteChecker.isBlack());
            checker.setId(whiteChecker.getId());
            checker.setKing(whiteChecker.isKing());
            checker.setField(whiteChecker.getField());
            checker.setToDrawX(whiteChecker.getToDrawX());
            checker.setToDrawY(whiteChecker.getToDrawY());
            
            allCheckerList.add(checker);
        }
        
        for(Checker blackChecker: blackList)
        {
            Checker checker = new Checker();
            checker.setBlack(blackChecker.isBlack());
            checker.setId(blackChecker.getId());
            checker.setKing(blackChecker.isKing());
            checker.setField(blackChecker.getField());
            checker.setToDrawX(blackChecker.getToDrawX());
            checker.setToDrawY(blackChecker.getToDrawY());
            
            allCheckerList.add(checker);
        }
        return allCheckerList;
    }
    
    public void showList()
    {
        for(int i = 0; i < checkersMovementsList.size(); i++)
        {
            for(Checker pionek: checkersMovementsList.get(i))
            {
                System.out.println(i + "nr pionka "+ pionek.getId() + " pole " + pionek.getField().getX1() + " " + pionek.getField().getY1());
            }
        }        
    }
    
    public List<String> findMovesInMovementsLists()
    {
        int numberOfMovement = 0;
        if(movementsToSave == null)
        {
            movementsToSave = new ArrayList();
        }
        else if (!movementsToSave.isEmpty())
        {            
            String[] number = movementsToSave.get(movementsToSave.size() - 1).split(" ");            
            numberOfMovement = Integer.valueOf(number[0]) + 1;
        }

        //musi zacząć od i = 1 by dostać sie do poprzedniej listy
        for(int i = 1 + numberOfMovement; i < checkersMovementsList.size(); i++)
        {
            int num = searchMovesAndKillsByComparison2Lists(checkersMovementsList.get(i-1),checkersMovementsList.get(i), numberOfMovement);            
            numberOfMovement += num;
        }
        return movementsToSave;
    }
    
    private String saveAsMoved(Checker checker, int numberOfMovement)
    {
        return  numberOfMovement + " " + String.valueOf(checker.isBlack()) + " " 
                                + String.valueOf(checker.isKing()
                                + " " + checker.getId() + " "
                                + checker.getField().getId());                                
    }
    
    private String saveAsKilled(Checker checker, int numberOfMovement)
    {
        return  numberOfMovement + " " + String.valueOf(checker.isBlack()) + " " 
                            + String.valueOf(checker.isKing()
                            + " " + checker.getId() + " " + "-1");
    }
    
    /**
     * To not repeat the code I return int isListEquals to know if I can't increment the 
     * number of movements. Equals means that it was no move.
     * It is easier than improve code in ControlGame.
     * 
     * 
     * @param checkersBefore
     * @param checkersAfter
     * @param numberOfMovement 
     */
    
    private int searchMovesAndKillsByComparison2Lists(List <Checker> checkersBefore, List<Checker> checkersAfter, int numberOfMovement)
    {
        boolean isListEquals = true;
        for(Checker checker1: checkersBefore)
        {
            boolean checkerExist = false;
            for(Checker checker2: checkersAfter)
            {
                if(checker2.equals(checker1) && !checker1.equalPosition(checker2))
                {
                    movementsToSave.add(saveAsMoved(checker2, numberOfMovement));
                    checkerExist = true;
                    isListEquals = false;
                }
                else if(checker2.equals(checker1) && checker1.equalPosition(checker2))
                {
                    checkerExist = true;
                    break;
                }                                    
            }
            if(!checkerExist)
            {
                movementsToSave.add(saveAsKilled(checker1, numberOfMovement));                
                isListEquals = false;
            }
        }
        if(isListEquals)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
    
    private Checker checkerExistInList(List <Checker> checkerList, Checker soughtChecker)
    {
        for(Checker checker: checkerList)
        {
            if(checker.equals(soughtChecker))
                return checker;            
        }
        return null;
    }
    
    /**
     * is the checker changed his position?
     * Funcion checks X1 position (Y1 is not necessary because checkers moves catacorner.)
     * 
     * 
     * @param checker
     * @param checker2
     * @return 
     */
    private boolean isCheckerMoved(Checker checker, Checker checker2)
    {
        if(checker.getField().getX1() != checker2.getField().getX1())
            return true;
        return false;
    }
    
    
    private String getFilename()
    {
        Calendar calendar = Calendar.getInstance(getLocale());
        
        return String.valueOf(calendar.get(Calendar.YEAR) + "-" +
                (calendar.get(Calendar.MONTH)+1) + "-" +
                calendar.get(Calendar.DAY_OF_MONTH) + " " + 
                calendar.get(Calendar.HOUR_OF_DAY) + "-" + 
                calendar.get(Calendar.MINUTE));
    }
    
    public void saveMovementsToFile(List <String> toSave, boolean isFinished) throws FileNotFoundException
    {
        PrintWriter save = null;
        if(isFinished)
        {
            boolean suc =  new File("gameHistory/").mkdir();
            if(new File("gameHistory/").isDirectory())
            {
                save = new PrintWriter("gameHistory/finished " + getFilename() + ".txt");                
            }
        }
        else
        {
            boolean suc =  new File("unfinishedGames/").mkdir();
            if(new File("unfinishedGames/").isDirectory())
            {
                save = new PrintWriter("unfinishedGames/" + getFilename() + "Moves.txt");                
            }
        }
        for(int i = 0; i < toSave.size(); i++)
        {
            save.println(toSave.get(i));
        }
        save.close();
    }
    
    public void saveGameObjects(List <Checker> whites, List<Checker> blacks, boolean whosFirst) throws FileNotFoundException
    {
        PrintWriter save = null;
        
        save = new PrintWriter("unfinishedGames/" + getFilename() + ".txt");
        save.println(whosFirst);
        for(Checker checker: whites)
        {
            save.println(checker.isBlack() + " " + checker.getId() + " " + checker.isKing() +
                         " " + checker.getField().getId());
        }       
        for(Checker checker: blacks)
        {
            save.println(checker.isBlack() + " " + checker.getId() + " " + checker.isKing() +
                         " " + checker.getField().getId());
        }
        save.close();        
    }    
    
    public void createHistoryFrame()
    {
        setSize(75*(boardSize*fieldSize)/100,85*(boardSize*fieldSize)/100);
        setLayout(null);        
        bChoosenFile.setBounds(150,10,150,20);        
        bChoosenFile.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {                
                fc = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter("TEXT FILES", "txt", "text");
                fc.setFileFilter(filter);
                fc.changeToParentDirectory();
                fc.showOpenDialog(fc);
                
                File file = fc.getSelectedFile();
                
                if(file != null)
                {
                    if(isFileCorrect(file))
                    {       
                        try 
                        {
                            forwardMovement = readMovementsFromFileToMove(file);                            
                            file = null;
                            historyPanel = new HistoryMovementsPanel(boardSize, (70*(fieldSize)/100));
                            historyPanel.setBounds(10,10,70*fieldSize/100*boardSize, 70*fieldSize/100*boardSize);

                            bNextMovement.setBounds(70*fieldSize/100*boardSize/2 + 30, 70*fieldSize/100*boardSize + 20, 50, 30);
                            bPreviousMovement.setBounds(70*fieldSize/100*boardSize/2 - 40, 70*fieldSize/100*boardSize + 20 , 50, 30);
                            
                            add(bNextMovement);
                            add(bPreviousMovement);
                            add(historyPanel);

                            repaint();                            
                        }
                        catch (FileNotFoundException ex) 
                        {
                            try {
                                ErrorHandling errorCall = new ErrorHandling();
                                errorCall.sendPOST(errorCall.getErrorDetails(ex));
                            } catch (IOException ex1) {
                            }
                        }
                    }
                    else
                    {
                        //DIALOG
                        System.out.println("Wczytałeś zły plik");
                    }                    
                }                 
            }
        });
        
        
        bNextMovement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                moveForward();                
                movementNumber++;
                repaint();
            }
        });
        
        bPreviousMovement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                movementNumber--;
                moveBack();
                repaint();
                
            }
        });                
        
        add(bChoosenFile);
        
        
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setVisible(true);
        
        this.addWindowListener(new WindowAdapter(){         
            @Override
            public void windowClosing(java.awt.event.WindowEvent w)
            {
                try {
                    clearObjects();
                } catch (Throwable ex) {
                    try {
                        ErrorHandling errorCall = new ErrorHandling();
                        errorCall.sendPOST(errorCall.getErrorDetails((Exception) ex));
                    } catch (IOException ex1) {
                    }
                }
            
            }
        });
    }
    
    public List <String> parseMoveToString(List <Move> moveList)
    {
        List <String> strMoveList = new ArrayList();
        for(Move move: moveList)
        {
            strMoveList.add(move.getIdMove() + " " + move.isBlack() + " " +
                    move.isKing() + " " + move.getIdChecker() + " " + move.getIdField());
        }    
        return strMoveList;
    }
    
    private void clearObjects() throws Throwable
    {
        setBackMovement(new ArrayList<Move>());
        setForwardMovement(null);
        movementNumber = 1;    
        
        if(historyPanel != null)
            getContentPane().remove(historyPanel);
        
        if(bChoosenFile.getActionListeners().length != 0)
           bChoosenFile.removeActionListener(bChoosenFile.getActionListeners()[0]);
        
        if(bNextMovement.getActionListeners().length != 0)
            bNextMovement.removeActionListener(bNextMovement.getActionListeners()[0]);
        
        if(bPreviousMovement.getActionListeners().length !=0)
            bPreviousMovement.removeActionListener(bPreviousMovement.getActionListeners()[0]);
        
        getContentPane().remove(bNextMovement);
        getContentPane().remove(bPreviousMovement);
        getContentPane().remove(bChoosenFile);
        
        
        
        //WHY?!?!?!?!
        /*historyPanel.checkerboard = null;
        historyPanel.rules = null;
        historyPanel = null;
        */
    }

    private void moveForward()
    {
        for(Move movement: forwardMovement)
        {
            if(movement.getIdMove() == movementNumber)
            {
                //szukaj pionka
                if(movement.isBlack())
                {
                    for(int i = 0; i < historyPanel.rules.getBlacks().size(); i++)
                    {
                        if(movement.getIdChecker() == historyPanel.rules.getBlacks().get(i).getId())
                        {     
                            Move copyMove = new Move(
                                movement.getIdMove(),
                                movement.isBlack(),
                                historyPanel.rules.getBlacks().get(i).isKing(),
                                historyPanel.rules.getBlacks().get(i).getId(),
                                historyPanel.rules.getBlacks().get(i).getField().getId());
                            
                            backMovement.add(copyMove);
                            changeArgumentsForward(historyPanel.rules.getBlacks().get(i), movement);
                        }
                    }
                }
                else
                {
                    for(int i = 0; i < historyPanel.rules.getWhites().size(); i++)
                    {
                        if(movement.getIdChecker() == historyPanel.rules.getWhites().get(i).getId())
                        {
                            Move copyMove = new Move(
                                movement.getIdMove(),
                                movement.isBlack(),
                                historyPanel.rules.getWhites().get(i).isKing(),
                                historyPanel.rules.getWhites().get(i).getId(),
                                historyPanel.rules.getWhites().get(i).getField().getId());
                            
                            backMovement.add(copyMove);
                            changeArgumentsForward(historyPanel.rules.getWhites().get(i), movement);
                        }
                    }                    
                }
                    
            }
            if(movement.getIdMove() != movementNumber && !forwardMovement.iterator().hasNext())
            {
                
            }
        }                
    }
    
    private void moveBack()
    {
        for(Move movement: backMovement)
        {
            if(movement.getIdMove() == movementNumber)
            {
                //szukaj pionka
                if(movement.isBlack())
                {
                    for(int i = 0; i < historyPanel.rules.getBlacks().size(); i++)
                    {
                        if(movement.getIdChecker() == historyPanel.rules.getBlacks().get(i).getId())
                        {                            
                            changeArgumentsBack(historyPanel.rules.getBlacks().get(i), movement);
                        }
                    }
                }
                else
                {
                    for(int i = 0; i < historyPanel.rules.getWhites().size(); i++)
                    {                        
                        if(movement.getIdChecker() == historyPanel.rules.getWhites().get(i).getId())
                        {
                            changeArgumentsBack(historyPanel.rules.getWhites().get(i), movement);
                        }
                    }                    
                }
                    
            }
        }        
    }
    
    public void changeArgumentsForward(Checker checker, Move movement)
    {
        checker.setKing(movement.isKing());
        if(movement.getIdField() != -1)
        {
            checker.setField(historyPanel.rules.getFieldById(movement.getIdField()));
        } 
        else
        {
            //STAŁA zamiana na "płynną"
            Field field = new Field();
            field.setX1(600);
            field.setY1(600);
            checker.setField(field);
        }
    }
    
    public void changeArgumentsBack(Checker pionek, Move movement)
    {
        pionek.setKing(movement.isKing());
        pionek.setField(historyPanel.rules.getFieldById(movement.getIdField()));
    }
    
    
    public boolean isFileCorrect(File file)
    {
        boolean result = false;
        
        try
        {
            Scanner in = new Scanner(file);
            String movement = in.nextLine();
            String[] movements = movement.split(" ");
            
            Move move = new Move(Integer.valueOf(movements[0]), Boolean.valueOf(movements[1]),
                    Boolean.valueOf(movements[2]),Integer.valueOf(movements[3]),
            Integer.valueOf(movements[4]));   
            result = true;                    
        }
        catch(FileNotFoundException | NullPointerException | NumberFormatException | NoSuchElementException e)
        {
            result = false;
            try {
                ErrorHandling errorCall = new ErrorHandling();
                errorCall.sendPOST(errorCall.getErrorDetails(e));
            } catch (IOException ex1) {
            }
        }

        return result;
    }
    
    public List <Move> readMovementsFromFileToMove(File file) throws FileNotFoundException
    {
        Scanner in = new Scanner(file);
        
        List <Move> movementsList = new ArrayList<>();
        do
        {
            String movement = in.nextLine();
            String[] movements = movement.split(" ");
            
            Move move = new Move(Integer.valueOf(movements[0]), Boolean.valueOf(movements[1]),
                    Boolean.valueOf(movements[2]),Integer.valueOf(movements[3]),
                    Integer.valueOf(movements[4]));
            
            movementsList.add(move);
        }
        while(in.hasNextLine());
        
        return movementsList;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        historyPanel.requestFocus();
        repaint();
    }

}
