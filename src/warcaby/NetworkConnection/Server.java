/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcaby.NetworkConnection;

import static com.oracle.jrockit.jfr.ContentType.StackTrace;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import warcaby.Checker;
import warcaby.Checkers;
import warcaby.Move;

/**
 *
 * @author Arkadio
 */
public class Server extends Thread{
    public static final int TEST = 165;
    public static final int PORT = 6000;
    public static final int BYTES_COMPANIES_IN_MAC = 3;
    
    public ServerSocket serverSocket;
    private final boolean blackfirst = false;
        
    private List<Checker> whiteCheckers, blackCheckers;                
    private Socket playerWhiteSocket, playerBlackSocket;
    private ServerVisibleThread serverVisibleThread;
    
    public Server() {               
        start();
    }
    
    
    @Override
    public void run() {
        try
        {
            serverVisibleThread = new ServerVisibleThread();
            Thread listenForPlayerRequest = new Thread(serverVisibleThread);
            listenForPlayerRequest.start();
            
            InetAddress iA = findLocalInterface();
            serverSocket = new ServerSocket(PORT, 30, iA);
            System.out.println("host adress " + serverSocket.getInetAddress().getHostAddress());

            //Podłączenie dwóch playerów
            //rozpoczęcie rozgrywki
            System.out.println("Server waits for players");
            playerWhiteSocket = serverSocket.accept();
            System.out.println("First player connected");
            
            sendMessage(playerWhiteSocket, "Hello on checkerboard server, we are waiting for other player");
            //Gracz zakładający wysyła pionki i planszę

            sendMessage(playerWhiteSocket, "checkers");
            // Tworzymy planszę + pionki

            blackCheckers = receiveCheckers(playerWhiteSocket);
            whiteCheckers = receiveCheckers(playerWhiteSocket);
            
            sendMessage(playerWhiteSocket, "colorW");
            sendMessage(playerWhiteSocket, "blockGame");           
            
            String message = "";
            
            while(!message.equals("yes"))
            {
                playerBlackSocket = serverSocket.accept();                    
                try{
                    sendMessage(playerBlackSocket, "doYouWantToPlay?");
                    message = receiveMessage(playerBlackSocket);
                }
                catch(EOFException e)
                {
                    ErrorHandling errorCall = new ErrorHandling();
                    errorCall.sendPOST(errorCall.getErrorDetails(e));
                    
                    System.out.println("Spoko nie ma problemu");
                }
                catch(SocketException e)
                {
                    ErrorHandling errorCall = new ErrorHandling();
                    errorCall.sendPOST(errorCall.getErrorDetails(e));
                
                    System.out.println("No pasa nada hijo");
                }
            }
            
            //WHITE
            if(receiveMessage(playerBlackSocket).equals("false"))
            {
                sendMessage(playerWhiteSocket, "colorB");
                Socket temp = playerBlackSocket;
                playerBlackSocket = playerWhiteSocket;
                playerWhiteSocket = temp;                                
            }
            
            //sendMessage(playerWhiteSocket, "Other player connected");
            //sendMessage(playerBlackSocket, "Hello on checkerboard server, we are waiting for other player");                        
            System.out.println("-----------------------");
            //załóżmy, że zakładający zaczyna
            
            
            while(!whiteCheckers.isEmpty() && !blackCheckers.isEmpty())
            {
                sendMessage(playerBlackSocket, "blockGame");
                sendMessage(playerWhiteSocket, "activateGame");
                
                do
                {
                    sendMessage(playerWhiteSocket, "sendMeMove");
                    List<Checker> checkersFromLastMove = receiveCheckerList(playerWhiteSocket);                                
                    sendMessage(playerBlackSocket, "addToCheckerList");
                    sendCheckerList(playerBlackSocket, checkersFromLastMove);

                    blackCheckers = receiveCheckers(playerWhiteSocket);
                    whiteCheckers = receiveCheckers(playerWhiteSocket);
                    int numberOfMoves = receiveNumberOfMoves(playerWhiteSocket);
                    List<Move> moveList = new ArrayList<>();

                    for(int i = 0; i < numberOfMoves; i++)
                    {                    
                        moveList.add(receiveMove(playerWhiteSocket));                                        
                        sendMessage(playerBlackSocket, "moveSent");
                        sendMove(playerBlackSocket, moveList.get(i));

                        receiveMessage(playerBlackSocket);
                    }
                }
                while(!receiveMessage(playerWhiteSocket).equals("endMove"));
                
                sendMessage(playerWhiteSocket, "blockGame");
                sendMessage(playerBlackSocket, "activateGame");                    
                //----------- pierwszy Ruch
                
                do
                {
                    sendMessage(playerBlackSocket, "sendMeMove");
                    List<Checker> checkersFromLastMove = receiveCheckerList(playerBlackSocket);                                
                    blackCheckers = receiveCheckers(playerBlackSocket);
                    whiteCheckers = receiveCheckers(playerBlackSocket);
                    int numberOfMoves = receiveNumberOfMoves(playerBlackSocket);
                    List<Move> moveList = new ArrayList<>();

                    sendMessage(playerWhiteSocket, "addToCheckerList");
                    sendCheckerList(playerWhiteSocket, checkersFromLastMove);

                    for(int i = 0; i < numberOfMoves; i++)
                    {
                        moveList.add(receiveMove(playerBlackSocket));

                        sendMessage(playerWhiteSocket, "moveSent");
                        sendMove(playerWhiteSocket, moveList.get(i));   

                        receiveMessage(playerWhiteSocket);
                    }       
                }
                while(!receiveMessage(playerBlackSocket).equals("endMove"));
                
                sendMessage(playerWhiteSocket, "activateGame");
                sendMessage(playerBlackSocket, "blockGame");
            }
                
            serverVisibleThread.setIsWorking(false);
            playerWhiteSocket.close();
            playerBlackSocket.close();  
        } 
        catch (IOException | ClassNotFoundException ex) 
        {
            serverVisibleThread.isWorking = false;
            serverVisibleThread.listenForRequest.close();
            try {
                serverSocket.close();
            } catch (IOException ex1) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex1);
            }
   
        }               
    }

    public Socket getPlayerWhiteSocket() {
        return playerWhiteSocket;
    }

    public Socket getPlayerBlackSocket() {
        return playerBlackSocket;
    }

    public ServerSocket getServerSocket() {
        return serverSocket;
    }
    
    
    public static InetAddress getBroadcastAddress(InetAddress iA) throws SocketException
    {
        InetAddress broadcast = null;
        Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
        while (en.hasMoreElements()) 
        {
            NetworkInterface ni = en.nextElement();            
            List<InterfaceAddress> list = ni.getInterfaceAddresses();
            Iterator<InterfaceAddress> it = list.iterator();
          
            while (it.hasNext()) 
            {
                InterfaceAddress ia = it.next();
                if(iA.getHostAddress().equals(ia.getAddress().getHostAddress()))
                {
                    broadcast = ia.getBroadcast();
                }
            }
        }
        return broadcast;
    }
    
    
    public static InetAddress findLocalInterface() throws SocketException
    {
        InetAddress iA = null;
        Enumeration e = NetworkInterface.getNetworkInterfaces();
        
        while(e.hasMoreElements())
        {
            NetworkInterface n = (NetworkInterface) e.nextElement();
            Enumeration ee = n.getInetAddresses();
            while (ee.hasMoreElements())
            {
                InetAddress inetAddress = (InetAddress) ee.nextElement();
                if(inetAddress.isLoopbackAddress())
                    break;
                if(isLocalAddress(inetAddress.getHostAddress()))
                {                   
                    byte mac[] = n.getHardwareAddress();

                    StringBuilder macStringBuilder = new StringBuilder();
                    for (int w = 0; w < BYTES_COMPANIES_IN_MAC; w++) 
                    {
                        macStringBuilder.append(String.format("%02X%s", mac[w], (w < BYTES_COMPANIES_IN_MAC - 1) ? ":" : ""));		
                    }
                    if(!isVirtualMachineAddress(macStringBuilder.toString()))
                        iA = inetAddress;
                }
            }
        }
        return iA;            
    }
    
    private static boolean isLocalAddress(String ipAddress)
    {
        String[] splitIP = ipAddress.split(Pattern.quote("."));
        
        if((splitIP[0].equals("192") && splitIP[1].equals("168")) || 
                splitIP[0].equals("10") || splitIP[0].equals("172"))
            return true;
        else
            return false;
    }
    
    //List of Virtual Machine's MAC addresses.    
    private static boolean isVirtualMachineAddress(String mac)
    {
        if(mac.equals("0A:00:27"))
            return true;
        else
            return false;
    }

    public void sendMessage(Socket socket, String message) throws IOException
    {
        ObjectOutputStream testToClient = new ObjectOutputStream(socket.getOutputStream());        
        testToClient.writeObject(message);
        testToClient.flush();        
        System.out.println("Server " + message);
    }
    
    public String receiveMessage(Socket socket) throws IOException, ClassNotFoundException
    {
        ObjectInputStream testToClient = new ObjectInputStream(socket.getInputStream());     
        String message = (String) testToClient.readObject();
        //System.out.println(message);
        return message;
    }
    
    public int receiveNumberOfMoves(Socket socket) throws IOException, ClassNotFoundException
    {
        ObjectInputStream testToClient = new ObjectInputStream(socket.getInputStream());     
        int number = testToClient.readInt();
       // System.out.println("Server: number of moves" + number);
        return number;
    }
        
    public void sendMove(Socket socket, Move move) throws IOException
    {
        ObjectOutputStream moveToClient = new ObjectOutputStream(socket.getOutputStream());        
        moveToClient.writeObject(move);
        moveToClient.flush();
    }
    
    public void sendCheckerList(Socket socket, List<Checker> checkerList) throws IOException 
    {
        ObjectOutputStream toServer = new ObjectOutputStream(socket.getOutputStream());        
        toServer.writeObject(checkerList);
        toServer.flush();        
    }
    
    public Move receiveMove(Socket socket) throws IOException, ClassNotFoundException
    {
        ObjectInputStream moveFromClient = new ObjectInputStream(socket.getInputStream()); 
        Move move = (Move) moveFromClient.readObject();
        //System.out.println("Server received move: " + move.getIdField());
        return move;
    }    
    
    public List<Checker> receiveCheckerList(Socket socket) throws IOException, ClassNotFoundException
    {
        ObjectInputStream checkerListFromClient = new ObjectInputStream(socket.getInputStream()); 
        List<Checker> checkerList = (List<Checker>) checkerListFromClient.readObject();
        //System.out.println("Server received checkerList, size: " + checkerList.size());
        return checkerList;
    }            
    
    public List<Checker> receiveCheckers(Socket socket) throws IOException, ClassNotFoundException
    {
        ObjectInputStream checkersFromClient = new ObjectInputStream(socket.getInputStream());        
        List<Checker> checkers = (List<Checker>) checkersFromClient.readObject();        
        //System.out.println("Server received checkers");
        return checkers;
    }    
    
    
}
