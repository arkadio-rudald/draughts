/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcaby.NetworkConnection;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import warcaby.Checker;
import warcaby.CheckerBoard;
import static warcaby.Checkers.boardSize;
import static warcaby.Checkers.fieldSize;
import warcaby.Field;
import warcaby.HistoryMovements;
import warcaby.Move;

/**
 *
 * @author Arkadio
 */
public class Player extends Thread {   
    
    public static final int PORT = 6000;
    private final int id;
    public static final int ID_HOST = 1;
    private Socket socket;
    private Move move;

    boolean isBusy = false;    
    boolean movesReceived = false;
    public boolean isOpenedConnection = false;
    public InetAddress iA;
    public ControlNetworkGame gameControl;    

    public boolean getSocket() {
        return socket.isConnected();
    }
       
    
    
    
    public Player(int id, int boardSize, int fieldSize) throws SocketException, IOException
    {        
        this.id = id;        
        iA = Server.findLocalInterface();
        System.out.println(iA.getCanonicalHostName());        
        
        isOpenedConnection = true;
        gameControl = new ControlNetworkGame(boardSize, fieldSize);
        //gameControl.checkerboard = gameControl.rotateBoard(gameControl.checkerboard);
    }            
    

    @Override
    public void run() {
        try 
        {                
            if(gameControl.isBlackFirst())
            {
                List<Checker> draughtsList = new ArrayList();
                draughtsList.addAll(gameControl.rules.getBlacks());
                draughtsList.addAll(gameControl.rules.getWhites());
                rotateBoardUsingCheckers(draughtsList);  
                
                gameControl.historyGame = new HistoryMovements(boardSize, fieldSize);
                gameControl.historyGame.getCheckerList().add(gameControl.historyGame.saveInCheckerLists(gameControl.rules.getWhites(), gameControl.rules.getBlacks()));
                
                gameControl.repaint();            
            }
            boolean gameIsOn = true;
            while(gameIsOn && isOpenedConnection)
            {
                listenStream();                
            }
        }
        catch (ClassNotFoundException | InterruptedException ex) {
            try {
                    ErrorHandling errorCall = new ErrorHandling();
                    errorCall.sendPOST(errorCall.getErrorDetails(ex));
                } catch (IOException ex1) {
            }
        }
        catch (IOException e)
        {
            try {
                socket.close();
                //maybe we can wait for reconnection, but is not necessary right now
            } catch (IOException ex) {
                Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    void rotateBoardUsingCheckers(List<Checker> checkerList)
    {
        int max = boardSize * boardSize - 1;
        
        for(Checker checker: checkerList)
        {
            if(checker.getId() != -1)
            {
                checker.setField(gameControl.rules.getFieldById(max - checker.getField().getId()));
            }
        }
    }
    
    Checker rotateChecker(Checker checker)
    {
        if(checker.getId() != -1)
        {
            int max = boardSize * boardSize - 1;

            Checker frontChecker = gameControl.rules.isCheckerOnBattleField(gameControl.rules.getFieldById(checker.getField().getId()));

            if (frontChecker != null)
            {
                frontChecker.setField(checker.getField());
                checker.setField(gameControl.rules.getFieldById(max - checker.getField().getId()));            
            }
            else
            {
                checker.setField(gameControl.rules.getFieldById(max - checker.getField().getId()));
            }
            gameControl.repaint();
        }
        return checker;
    }
    
    Move rotateMove(Move move)
    {
        if (move.getIdField() != -1)
        {
            int max = boardSize * boardSize - 1;
            move.setIdField(max - move.getIdField());
        }
        return move;
    }
    
    
        
    
    public void listenStream() throws IOException, ClassNotFoundException, InterruptedException
    {
        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
        String message = String.valueOf(ois.readObject());
        
        System.out.println("Player " + id + " listened: " + message);                
        
        if(message.equals("checkerboard"))
            sendCheckerBoard(gameControl.checkerboard);
        
        else if(message.equals("checkers"))
        {
            sendCheckers(gameControl.rules.getBlacks());
            sendCheckers(gameControl.rules.getWhites());                        
        }            
        else if(message.equals("colorB"))
        {
            setColour(true);
            
            List<Checker> draughtsList = new ArrayList();
            draughtsList.addAll(gameControl.rules.getBlacks());
            draughtsList.addAll(gameControl.rules.getWhites());
            rotateBoardUsingCheckers(draughtsList);  
            
            gameControl.historyGame = new HistoryMovements(boardSize, fieldSize);
            gameControl.historyGame.getCheckerList().add(gameControl.historyGame.saveInCheckerLists(gameControl.rules.getWhites(), gameControl.rules.getBlacks()));
                
            gameControl.repaint();
        }
        else if(message.equals("colorW"))
            setColour(false);
        else if(message.equals("blockGame"))
            gameControl.isBlock = true;
        else if(message.equals("activateGame"))
            gameControl.isBlock = false;
        else if(message.equals("sendMeMove"))
        {
            gameControl.setPlayerThread(Thread.currentThread());
            
            waitingForMove();           
            
            List<String> lastMove = gameControl.historyGame.findMovesInMovementsLists();
            List<Move> moveList = findLastMoves(lastMove);            
            sendCheckerList(gameControl.historyGame.getCheckerList().get(gameControl.historyGame.getCheckerList().size() - 1));
            sendCheckerList(gameControl.rules.getBlacks());
            sendCheckerList(gameControl.rules.getWhites());
            
            List<Checker> biale = gameControl.rules.getWhites();
            List<Checker> czarne = gameControl.rules.getBlacks();
            List<Checker> historia = gameControl.historyGame.getCheckerList().get(gameControl.historyGame.getCheckerList().size() - 1);
            
            sendNumberOfMoves(moveList.size());
            for(int i = 0; i < moveList.size(); i++)
            {
                sendMove(moveList.get(i));
            }            
            
            if(gameControl.endMove)
            {
                sendMessage("endMove");
                gameControl.endMove = false;
            }                
            else
                sendMessage("isMoving");

        }
        else if(message.equals("addToCheckerList"))
        {
            List<Checker> checkers = receiveCheckerList();
            
            if((gameControl.isBlackFirst()) || socket.getLocalAddress().getHostAddress().equals(iA.getHostAddress()))
                rotateBoardUsingCheckers(checkers);                
            
            gameControl.historyGame.getCheckerList().add(checkers);
            
        }
        else if(message.equals("moveSent"))
        {
            move = receiveMove();
            move = rotateMove(move);
            Checker movedChecker = gameControl.rules.getCheckerByIdAndColour(move.getIdChecker(), move.isBlack());
            //tutaj powinienem zmienić pole jeśli jest czarnym
            /*if (gameControl.isBlackFirst())
            {
                rotateChecker(movedChecker);            
            }*/
            
            if(move.getIdField() == -1)
            {
                Field field = gameControl.rules.checkField(movedChecker.getField().getX1(), movedChecker.getField().getY1());
                field.setFree(true);
                gameControl.rules.removeChecker(movedChecker);
            }
            else
            {    
                gameControl.rules.movingChecker(movedChecker, movedChecker.getField(), gameControl.rules.getFieldById(move.getIdField()));
                movedChecker.setKing(move.isKing());
            }            
            gameControl.repaint();            
            sendMessage("moveReceived"); 
        }          
        else if(message.equals("doYouWantToPlay?"))
        {
            //Guest choose the colour
            sendMessage("yes");
            sendMessage(String.valueOf(gameControl.isBlackFirst()));
        }
        else if(message.equals("closeConnection"))
        {
            isOpenedConnection = false;
            socket.close();
        }
            
    }

    public void setSocket(int number) throws IOException {
        String ipAddress = iA.getHostAddress();
        String[] ipNumbers = ipAddress.split(Pattern.quote("."));
        String subnet = ipNumbers[0] + "." + ipNumbers[1] + "." + ipNumbers[2] + ".";                
        this.socket = scanForServerByIpAddress(subnet, number);
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }        
    
    public Socket scanForServerByIpAddress(String subnet, int number) throws IOException
    {
        Socket socket = null;        
        for(int i = number; i < 255; i++)
        {
            socket = new Socket(subnet + i, 6000);                                        
        }
        return socket;
    }
    
    
    
    public synchronized boolean waitingForMove() throws InterruptedException
    {
        synchronized(Thread.currentThread())
        {
            wait();
        }
        return true;
    }
    
    
    public List<Move> findLastMoves(List<String> list)
    {
        List<Move> moveList = new ArrayList<Move>();
        int max = 0;
        for(int i = 0; i < list.size(); i++)
        {
            String[] move = list.get(i).split(" ");            
            if(max < Integer.valueOf(move[0]))
                max = Integer.valueOf(move[0]);
        }
                
        for(int i = 0; i < list.size(); i++)
        {
            String[] move = list.get(i).split(" ");            
            if(Integer.valueOf(move[0]) == max)
            {
                Move mov = new Move(Integer.valueOf(move[0]), Boolean.valueOf(move[1]),
                    Boolean.valueOf(move[2]),Integer.valueOf(move[3]),
                Integer.valueOf(move[4]));                   
                moveList.add(mov);
            }
        }
        return moveList;
    }
    
    public void connectWithServer(String ipAddress) throws IOException
    {
        socket = new Socket(ipAddress, PORT);
        isOpenedConnection = true;     
    }
    
    public void broadcastTestStream() throws IOException
    {
        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);        
//        out.println("Player " + id + " sent a message");
    }
    
    public void sendMessage(String message) throws IOException
    {
        ObjectOutputStream toServer = new ObjectOutputStream(socket.getOutputStream());        
        toServer.writeObject(message);
        toServer.flush();        
//        System.out.println("Player " + id + " sent a message");
    }
    
    public void sendColour(boolean isBlack) throws IOException
    {
        ObjectOutputStream toServer = new ObjectOutputStream(socket.getOutputStream());        
        toServer.writeBoolean(isBlack);
        toServer.flush();        
    }
    
    private void sendCheckerList(List<Checker> checkerList) throws IOException 
    {
        ObjectOutputStream toServer = new ObjectOutputStream(socket.getOutputStream());        
        toServer.writeObject(checkerList);
        toServer.flush();        
    }
    
    public void sendNumberOfMoves(int number) throws IOException
    {
        ObjectOutputStream toServer = new ObjectOutputStream(socket.getOutputStream());        
        toServer.writeInt(number);
        toServer.flush();        
        //System.out.println("Player " + id + " sent a number " + number );
    }
    
    public void sendMove(Move move) throws IOException
    {
        ObjectOutputStream toServer = new ObjectOutputStream(socket.getOutputStream());        
        toServer.writeObject(move);
        toServer.flush();    
    }
    
    public Move receiveMove() throws IOException, ClassNotFoundException
    {
        ObjectInputStream moveFromServer = new ObjectInputStream(socket.getInputStream());
        Move move = (Move) moveFromServer.readObject();
        return move;
    }
    
    public List<Checker> receiveCheckerList() throws IOException, ClassNotFoundException
    {
        ObjectInputStream checkerListFromClient = new ObjectInputStream(socket.getInputStream()); 
        List<Checker> checkerList = (List<Checker>) checkerListFromClient.readObject();        
        return checkerList;
    }    
    
    public void sendCheckerBoard(CheckerBoard checkerboard) throws IOException
    {
        ObjectOutputStream checkerBoardToServer = new ObjectOutputStream(socket.getOutputStream());        
        checkerBoardToServer.writeObject(checkerboard);
        checkerBoardToServer.flush();
    }
    
    public void sendCheckers(List<Checker> checkers) throws IOException
    {
        ObjectOutputStream checkersToServer = new ObjectOutputStream(socket.getOutputStream());        
        checkersToServer.writeObject(checkers);
        checkersToServer.flush();
    }
    
    public int receiveNumberOfMoves() throws IOException, ClassNotFoundException
    {
        ObjectInputStream testToClient = new ObjectInputStream(socket.getInputStream());     
        int number = testToClient.readInt();
        System.out.println(number);
        return number;
    }
    
    public void setColour(boolean color)
    {
        gameControl.setBlackFirst(color);
    }           
    
}
