package warcaby.NetworkConnection;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Arkadio
 */
public class ErrorHandling {
    private static final String url = "http://bycniebytem.cba.pl/warcaby/errors.php";
     
    public ErrorHandling()
    {
    }

    
    public String getErrorDetails(Exception e)
    {
        String message = null;
        
        StackTraceElement[] stack  = e.getStackTrace();
        
        message = e.getMessage() + " ";
        for(int i = 0; i < stack.length; i++)
        {
            message += stack[i].getClassName() + "."
                    + stack[i].getMethodName() + ":"
                    + stack[i].getLineNumber() + "\r\n";
        }
        message += "---------------------------------------------------------\r\n\r\n";
        return message;
    }
    
    
    
    public void sendPOST(String message) throws IOException {
        Map<String, String> params = new HashMap<String, String>();
        params.put("Error", message);
         
        try {
            HttpUtility.sendPostRequest(url, params);            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        HttpUtility.disconnect();
    }
}
