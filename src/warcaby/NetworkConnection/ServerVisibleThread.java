/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcaby.NetworkConnection;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Arkadio
 */
public class ServerVisibleThread implements Runnable{
    
    protected DatagramSocket listenForRequest;
    public boolean isWorking = true;
    private final String RESPONSE;
    private final String REQUEST;
    private final int SIZE_OF_MESSAGE_RECEIVED;
    
    public ServerVisibleThread() {
        this.RESPONSE = "draughtsMan";
        this.REQUEST = "AreYouHostingDraughts?";
        this.SIZE_OF_MESSAGE_RECEIVED = 100;
    }
    
    @Override
    public void run() {
        byte [] receivedMessageInformation = new byte [SIZE_OF_MESSAGE_RECEIVED];
        try {
            listenForRequest = new DatagramSocket(Server.PORT - 1);    
            //Datagram to receive request
            DatagramPacket packet;
            
            while(isWorking)
            {
                packet = new DatagramPacket(receivedMessageInformation, receivedMessageInformation.length);
                String receivedMessage = "";
                listenForRequest.receive(packet);
                receivedMessageInformation = packet.getData();
                char inf;
                
                for(int i = 0; i < packet.getLength(); i++)
                {
                    inf = (char)receivedMessageInformation[i];
                    receivedMessage = receivedMessage.concat(String.valueOf(inf));
                }
                
                System.out.println("packet from " + packet.getAddress() + " "+ receivedMessage);    
                if(receivedMessage.equals(REQUEST))
                {
                    //Datagram to send a response
                    packet = new DatagramPacket(RESPONSE.getBytes(), RESPONSE.length(), packet.getAddress() , Server.PORT-1);
                    listenForRequest.send(packet);
                }
            }
            listenForRequest.close();
        } catch (IOException ex) {            
            listenForRequest.close();            
        }
    }

    public void setIsWorking(boolean isWorking) {
        this.isWorking = isWorking;
    }        
}
