/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcaby.NetworkConnection;

import java.awt.Dimension;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
/**
 *
           
    1. pobranie datagramu, sprawdzenie go
    2. dodanie hosta do listy,
    3. wyświetlenie hosta, czekanie na odpowiedź aż do TIMEOUT = 15 sekund (to wiąże się z nowym wątkiem).
            
            
            
 * @author Arkadio
 */
public class ServerScannerBroadcast implements Runnable{

    public static final int TIMEOUT = 10000;
    private final InetAddress iA;
    private final InetAddress broadcastAddress;
    private DatagramSocket searchForServerSocket;
    
    private List<JButton> serverButtons;    
    public JPanel serverListPanel;
    
    private final String RESPONSE;
    private final String REQUEST;
    private final int SIZE_OF_MESSAGE_RECEIVED;
    boolean isWaitingForResponse = false;
    
    
    public ServerScannerBroadcast(JPanel gamePanel) throws SocketException
    {
        this.RESPONSE = "draughtsMan";
        this.REQUEST = "AreYouHostingDraughts?";
        this.SIZE_OF_MESSAGE_RECEIVED = 100;
        
        iA = Server.findLocalInterface();      
        broadcastAddress = Server.getBroadcastAddress(iA);                      
        serverListPanel = gamePanel;
        serverButtons = new ArrayList<JButton>();
    }
    
    
    @Override
    public void run() {
        isWaitingForResponse = true;
        byte[] receivedMessageInformation = new byte[SIZE_OF_MESSAGE_RECEIVED];
        
        DatagramPacket packet;
            try
            {
                searchForServerSocket = new DatagramSocket(Server.PORT - 1);
                packet = new DatagramPacket(REQUEST.getBytes(), REQUEST.length(), broadcastAddress, Server.PORT - 1);
                searchForServerSocket.send(packet);
            }
            catch (SocketException ex) {
                packet = new DatagramPacket(receivedMessageInformation,receivedMessageInformation.length);                
                Logger.getLogger(ServerScannerBroadcast.class.getName()).log(Level.SEVERE, null, ex);
            }
            catch (IOException ex) 
            {   
                packet = new DatagramPacket(receivedMessageInformation, receivedMessageInformation.length);                
                Logger.getLogger(ServerScannerBroadcast.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            while(isWaitingForResponse)
            {            
                try 
                {
                    searchForServerSocket.setSoTimeout(TIMEOUT);
                    searchForServerSocket.receive(packet);                    
                }
                catch (IOException ex) 
                {
                    isWaitingForResponse = false;
                    searchForServerSocket.close();
                    System.out.println("Looking for server is done!");                    
                    continue;
                }
                String receivedMessage = "";
                char inf;
            
                for(int i = 0; i < packet.getLength(); i++)
                {
                    receivedMessageInformation = packet.getData();
                    inf = (char)receivedMessageInformation[i];
                    receivedMessage = receivedMessage.concat(String.valueOf(inf));                    
                }
                
                if(receivedMessage.equals(RESPONSE))
                {            
                    System.out.println("Response: " + RESPONSE);
                    String address = packet.getAddress().getHostAddress();
                    String hostname = packet.getAddress().getHostAddress();
                    addButtonsToPanel(address, hostname);                    
                }            
            }
        }
    
    public void addButtonsToPanel(String address, String serverName)
    {                
        JButton serverButton = new JButton(serverName);        
        ButtonListenerJoinedPlayer buttonListener = new ButtonListenerJoinedPlayer(address, serverListPanel);  
        serverButton.addActionListener(buttonListener);
        
        serverButton.setBounds(10,serverButtons.size()*45, 150, 40);
        serverButtons.add(serverButton);           
        serverListPanel.add(serverButton);
        System.out.println("I added a button");
       
        JFrame parentFrame = (JFrame) SwingUtilities.getWindowAncestor(serverListPanel);
        Dimension frameDimension = parentFrame.getSize();
        parentFrame.setPreferredSize(frameDimension);
        serverButton.setPreferredSize(new Dimension(150,40));
        serverButton.setSize(150,40);
        serverListPanel.setPreferredSize(serverListPanel.getSize());   
        parentFrame.setSize(frameDimension);
    
            parentFrame.pack();
        serverListPanel.repaint();   
             
    }
        
    public List<JButton> getServerButtons() {
        return serverButtons;
    }
}
