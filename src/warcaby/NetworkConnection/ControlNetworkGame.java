/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcaby.NetworkConnection;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import warcaby.Checker;
import warcaby.CheckerBoard;
import warcaby.Field;
import warcaby.HistoryMovements;
import warcaby.Principle;

/**
 *
 * @author Arkadio
 */
public class ControlNetworkGame extends JPanel implements MouseListener, MouseMotionListener {

    private final Color black = new Color(0, 120, 0);
    private final Color white = new Color(255, 255, 128);

    private boolean blackFirst = false;
    private Checker lastChecker;
    private Field markedField;
    private Field lastField;
    private boolean wasBattle;
    private boolean wasClicked;
    private boolean isMouseDragged;
    boolean noBattleAfterBecomeAKing = false;
    
    public CheckerBoard checkerboard;
    Principle rules;
    
    //Network
    boolean endMove = false;
    public boolean isBlock = false;
    
    boolean testDragged = false;
    
    HistoryMovements historyGame;
    private final int boardSize;
    private final int fieldSize;
    
    Thread playerThread;
    
            
    public ControlNetworkGame(int boardSize, int fieldSize)
    {
        this.fieldSize = fieldSize;
        this.boardSize = boardSize;
        
        checkerboard = new CheckerBoard(boardSize, fieldSize);         
        historyGame = new HistoryMovements(boardSize, fieldSize);
        rules = new Principle(checkerboard.getBoard(), boardSize, fieldSize);
        rules.netGame = true;
        historyGame.getCheckerList().add(historyGame.saveInCheckerLists(rules.getWhites(), rules.getBlacks()));                   
    }
    
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        
        checkerboard.drawBoard(g);
        if(!blackFirst)
        {
            for(Checker k: rules.getBlacks())
            {
                g.setColor(black);
                k.drawChecker(g, fieldSize);
            }
            for(Checker k: rules.getWhites())
            {
                g.setColor(white);
                k.drawChecker(g, fieldSize);
            }        
        }
        else
        {
            for(Checker k: rules.getWhites())
            {
                g.setColor(white);
                k.drawChecker(g, fieldSize);
            }
            for(Checker k: rules.getBlacks())
            {
                g.setColor(black);
                k.drawChecker(g, fieldSize);
            }            
        }
    }
    
    //board and I return board
    CheckerBoard rotateBoard(CheckerBoard board)
    {
        CheckerBoard result = new CheckerBoard(boardSize,fieldSize);
        
        int i = boardSize*boardSize - 1;
        for(Field field: board.checkerboard)
        {
            Field copyField = new Field();
            copyField.setForbidden(field.isForbidden());
            copyField.setFree(field.isFree());
            copyField.setId(i);
            copyField.setX1(field.getX1());
            copyField.setX2(field.getX2());
            copyField.setY1(field.getY1());
            copyField.setY2(field.getY2());
            
            result.checkerboard.add(copyField);
            i--;
        }
        return result;
    }

        
    @Override
    public void mouseClicked(MouseEvent e) {
    }   

    @Override
    public void mousePressed(MouseEvent e) {
        
        if(!isBlock)
        {
            //sprawdzenie i zablokowanie pionków drużyny przeciwnej
            if(blackFirst)
            {
                rules.blockadeCheckers(rules.getWhites());
                rules.activateCheckers(rules.getBlacks());
            }
            else
            {
                rules.blockadeCheckers(rules.getBlacks());
                rules.activateCheckers(rules.getWhites());
            }

            lastField = markedField;
            markedField = rules.checkField(e.getX(), e.getY());

            //sprawdzam czy na polu znajduje się pionek
            if(rules.getCheckerFromField(markedField) != null)
                rules.setMarkedChecker(rules.getCheckerFromField(markedField));
            

            //jeśli istnieje pionek zaznaczony to sprawdzamy czy 
            //ma bicie 

            if(rules.getMarkedChecker() != null)
            {
                System.out.println("Mouse Pressed Checker: " + rules.getMarkedChecker().getId() + " Field: " + markedField.getId());
                rules.blockadeFreeFields();

                if(!rules.isBattle(blackFirst).isEmpty() && !noBattleAfterBecomeAKing)
                {
                    wasBattle = true;
                    if(lastChecker != null)
                    {
                        if(rules.getMarkedChecker().getId() != lastChecker.getId())
                            rules.blockadeFreeFields();                        
                    }                
                }
                else
                {                
                    rules.activateAllFreePossiblyFields();
                    rules.blockadeImpossiblyFieldToPutChecker(rules.getMarkedChecker());                
                }
            }
            else
                System.out.println("Mouse Pressed Checker: null " + " Field: " + markedField.getId());
            
            if(lastField == null)
                lastField = new Field();
            
            testDragged = false;
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
        if(!isBlock)
        {
            testDragged = false;
            if(isMouseDragged)
            {
                System.out.println("Mouse Released Checker: " + rules.getMarkedChecker().getId() + " Field: " + rules.checkField(e.getX(), e.getY()).getId());
                System.out.println("--------------");
                
                
                Field theSameField = rules.getMarkedChecker().getField();
                if((rules.getMarkedChecker().getField().getId() == rules.checkField(e.getX(), e.getY()).getId()
                        && !markedField.isForbidden() && rules.checkField(e.getX(), e.getY()).isFree()))
                {
                    rules.getMarkedChecker().setToDrawX(rules.getMarkedChecker().getField().getX1());
                    rules.getMarkedChecker().setToDrawY(rules.getMarkedChecker().getField().getY1());

                    repaint();
                }
                else
                {
                    if(rules.getMarkedChecker() != null)
                    {
                        if(rules.isCorrectField(rules.checkField(e.getX(), e.getY()), rules.getMarkedChecker()))
                        {
                            rules.getMarkedChecker().getField().setX1(rules.checkField(e.getX(), e.getY()).getX1());
                            rules.getMarkedChecker().getField().setY1(rules.checkField(e.getX(), e.getY()).getY1());
                            rules.getMarkedChecker().setToDrawX(rules.checkField(e.getX(), e.getY()).getX1());
                            rules.getMarkedChecker().setToDrawY(rules.checkField(e.getX(), e.getY()).getY1());

                            rules.activateAllFreePossiblyFields();
                            markedField.setFree(true);
                            markedField.setForbidden(false);

                            if(!wasClicked)
                            {
                                lastField = markedField;
                            }


                            if(!isMouseDragged)
                                rules.setMarkedChecker(rules.getCheckerFromField(markedField));            

                            // Sprawdzenie czy obecny pionek ma bicie
                            if(rules.isBattle(rules.getMarkedChecker()) && wasBattle)
                            {
                                markedField = rules.checkField(e.getX(), e.getY());
                                if(wasBattle)
                                {
                                    try{
                                        rules.removeTakenChecker(rules.getMarkedChecker(), markedField, lastField);
                                    }
                                    catch(NullPointerException es)
                                    {                       
                                        try {
                                            ErrorHandling errorCall = new ErrorHandling();
                                            errorCall.sendPOST(errorCall.getErrorDetails(es));
                                        } catch (IOException ex1) {
                                        }
                                    }
                                }
                                lastChecker = rules.getMarkedChecker();      
                                lastField = markedField;          
                                repaint();                           
                            }
                            lastField = markedField;

                            rules.activateAllFreePossiblyFields();

                            noBattleAfterBecomeAKing = rules.getMarkedChecker().isKing();
                        
                            rules.isCheckerToBeAKing(rules.getMarkedChecker(), wasBattle);
                            if(rules.getMarkedChecker().isKing() != noBattleAfterBecomeAKing)
                                noBattleAfterBecomeAKing = true;
                            else
                                noBattleAfterBecomeAKing = false;

                            if(isMouseDragged)
                            {
                                rules.checkField(e.getX(), e.getY()).setForbidden(false);
                                rules.checkField(e.getX(), e.getY()).setFree(false);
                            }

                            if(!noBattleAfterBecomeAKing)
                            {   //Sprawdzenie czy ten sam pionek ma kolejne bicia
                                if(!(rules.isBattle(rules.getMarkedChecker()) && wasBattle))
                                {
                                    lastChecker = null;
                                    rules.setMarkedChecker(null);     
                                    wasBattle = false;       
                                    endMove = true;
                                }
                            }
                            else
                            {
                                lastChecker = null;
                                rules.setMarkedChecker(null);     
                                wasBattle = false;  
                                noBattleAfterBecomeAKing = false;
                                endMove = true;
                            }
                            repaint();   
                        }
                        else
                        {
                            rules.getMarkedChecker().setToDrawX(rules.getMarkedChecker().getField().getX1());
                            rules.getMarkedChecker().setToDrawY(rules.getMarkedChecker().getField().getY1());                        
                            rules.setMarkedChecker(null);
                            repaint();
                        }
                    }
                    if(theSameField.getId() != rules.checkField(e.getX(), e.getY()).getId())
                    {
                        historyGame.getCheckerList().add(historyGame.saveInCheckerLists(rules.getWhites(), rules.getBlacks()));
                        wakePlayerThread(playerThread);                    
                    }
                }    
                wasClicked = false;
            }   
            else
            {
                if(rules.getMarkedChecker() != null)
                {
                    if(rules.isCorrectField(rules.checkField(e.getX(), e.getY()), rules.getMarkedChecker()))
                    {
                        wasClicked = true;
                        System.out.println("Mouse Released Checker: " + rules.getMarkedChecker().getId() + " Field: " + rules.checkField(e.getX(), e.getY()).getId());
                        System.out.println("--------------");   

                        rules.activateAllFreePossiblyFields();
                        markedField.setFree(false);
                        markedField.setForbidden(false);
                        rules.setMarkedChecker(rules.getCheckerFromField(markedField));            
                        if(wasBattle)
                        {
                            rules.removeTakenChecker(rules.getMarkedChecker(), markedField, lastField);
                        }

                        rules.activateAllFreePossiblyFields();
                        if(rules.isBattle(rules.getMarkedChecker()) && wasBattle)
                        {
                            lastChecker = rules.getMarkedChecker();      
                            lastField = markedField;          
                        }
                        else
                        {
                            if(lastChecker != null)
                                rules.isCheckerToBeAKing(lastChecker, wasBattle);
                            
                            lastChecker = null;
                            rules.setMarkedChecker(null);     
                            wasBattle = false;
                            wasClicked = false;                                
                            noBattleAfterBecomeAKing = false;
                            endMove = true;                            
                        }         
                        historyGame.getCheckerList().add(historyGame.saveInCheckerLists(rules.getWhites(), rules.getBlacks()));                               
                        wakePlayerThread(playerThread);
                        
                        repaint();
                    }
                    else if(rules.getMarkedChecker().getField().getId() == rules.checkField(e.getX(), e.getY()).getId())
                    {
                        wasClicked = true;
                    }
                    else
                    {
                        markedField = lastField;
                        wasClicked = false;
                        System.out.println("Mouse Released Checker: null " + " Field: " + rules.checkField(e.getX(), e.getY()).getId());
                        System.out.println("--------------");

                    }
                }
            }        
            isMouseDragged = false;        

            //warunek końca
            if(rules.getWhites().isEmpty() || rules.getBlacks().isEmpty())
            {
                if(!rules.getWhites().isEmpty())
                {
                    //Pokaż okno dialogowe białe wygrały
                }
                else
                {
                    //Pokaż okno dialogowe czarne wygrały
                }

                try {
                    historyGame.saveMovementsToFile(historyGame.findMovesInMovementsLists(), true);
                    //KONIEC
                    //zapisujemy historię, wskazujemy wygranego
                } catch (FileNotFoundException ex) {
                    try {
                        ErrorHandling errorCall = new ErrorHandling();
                        errorCall.sendPOST(errorCall.getErrorDetails(ex));
                    } catch (IOException ex1) {
                    }
                }
            }          
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
    
    public synchronized void wakePlayerThread(Thread player)
    {
        synchronized(player)
        {
            player.notify();        
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if(!testDragged)
            System.out.println("Mouse Dragged");
        if(rules.getMarkedChecker() != null)
        {
            //STAŁA
            rules.getMarkedChecker().setToDrawX(e.getX()-(fieldSize)/2);
            rules.getMarkedChecker().setToDrawY(e.getY()-(fieldSize)/2);
            repaint();
            isMouseDragged = true;
        }
        testDragged = true;
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        //System.out.println("Mouse Moved");
    }
    
    
    /**
     * Loading Game from txt file:
     * first line: "Is black next?" true or false
     * next lines are checkers in this order
     * isBlack, CheckerId, isKing, FieldId, 
     * 
     * 
     * @param file
     * @throws FileNotFoundException 
     */
    
    
    public void loadGame(File file) throws FileNotFoundException
    {
        
        //dodać obiekty do listy ruchów z pliku unfinished
        //parsowanie
        Scanner in = new Scanner(file);
        boolean whosFirst = Boolean.valueOf(in.nextLine());
        
        char[] filename = file.getPath().toCharArray();
        
        filename[filename.length-4] = 'M';
        filename[filename.length-3] = 'o';
        filename[filename.length-2] = 'v';
        filename[filename.length-1] = 'e';
        
        
        String flname = String.valueOf(filename);
        flname = flname.concat("s.txt");
        
        
        File fileHistory = new File(flname);
        historyGame.setMovementsToSave(historyGame.parseMoveToString(historyGame.readMovementsFromFileToMove(fileHistory)));
        
        
        List <Checker> blackCheckers = new ArrayList();
        List <Checker> whiteCheckers = new ArrayList();
        
        do
        {
            String[] checkerData = in.nextLine().split(" ");
            Checker checker = new Checker();
            checker.setBlack(Boolean.valueOf(checkerData[0]));
            checker.setId(Integer.valueOf(checkerData[1]));
            checker.setKing(Boolean.valueOf(checkerData[2]));
            checker.setField(rules.getFieldById(Integer.valueOf(checkerData[3])));
            rules.getFieldById(Integer.valueOf(checkerData[3])).setFree(false);

            
            if(whosFirst)
            {
                if(!checker.isBlack())
                {
                    rules.getFieldById(Integer.valueOf(checkerData[3])).setForbidden(true);
                }                
            }
            else
            {
                if(checker.isBlack())
                {
                    rules.getFieldById(Integer.valueOf(checkerData[3])).setForbidden(true);
                }
            }
            
                    
            if(checker.isBlack())
            {
                blackCheckers.add(checker);
            }
            else
            {
                whiteCheckers.add(checker);
            }            
        }
        while(in.hasNext());
        
        blackFirst = whosFirst;
        rules.setBlacks(blackCheckers);
        rules.setWhites(whiteCheckers);
        rules.changeFieldsToFreeIfEmpty();
        repaint();       
    }    
    
    public boolean isFileCorrect(File file)
    {
        boolean result = false;
        
        try
        {
            Scanner in = new Scanner(file);
            boolean whosFirst = Boolean.valueOf(in.nextLine());
            String[] checkerData = in.nextLine().split(" ");
            Checker checker = new Checker();
            checker.setBlack(Boolean.valueOf(checkerData[0]));
            checker.setId(Integer.valueOf(checkerData[1]));
            checker.setKing(Boolean.valueOf(checkerData[2]));
            checker.setField(rules.getFieldById(Integer.valueOf(checkerData[3])));
            
            result = true;                    
        }
        catch(FileNotFoundException | NullPointerException | NumberFormatException | NoSuchElementException e)
        {
            result = false;
        }

        return result;
    }

    public boolean isBlackFirst() {
        return blackFirst;
    }
    
    public void setBlackFirst(boolean blackFirst)
    {
        this.blackFirst = blackFirst;
    }

    public void setPlayerThread(Thread playerThread) {
        this.playerThread = playerThread;
    }       
}