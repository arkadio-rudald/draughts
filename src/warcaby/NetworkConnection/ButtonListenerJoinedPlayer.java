/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcaby.NetworkConnection;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import static warcaby.Checkers.boardSize;
import static warcaby.Checkers.fieldSize;



/**
 *
 * @author Arkadio
 */
public class ButtonListenerJoinedPlayer implements ActionListener{
    private final String serverIp;
    private Player player;
    private final JPanel serverListPanel;
    private JFrame parentFrame;
    
    
    public ButtonListenerJoinedPlayer(String serverIp, JPanel serverListPanel) {        
        this.serverListPanel = serverListPanel;
        this.serverIp = serverIp;       
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        try
        {
            serverListPanel.removeAll();            
            JPanel bigPanel = new JPanel();
            bigPanel.setLayout(null);
            
            player = new Player(2,boardSize, fieldSize);
            player.gameControl.setBounds(10, 10, boardSize*fieldSize , boardSize*fieldSize);
            player.gameControl.addMouseListener(player.gameControl);
            player.gameControl.addMouseMotionListener(player.gameControl);
            player.gameControl.setFocusable(true);

            player.gameControl.setPreferredSize(new Dimension(boardSize*fieldSize,boardSize*fieldSize));
            player.gameControl.setLayout(null);
            player.connectWithServer(serverIp);
            System.out.println("Is connected with server? " + player.getSocket());
            
            Dimension gameDimension = player.gameControl.getPreferredSize();
            
            JButton whiteButton = new JButton("White");
            JButton blackButton = new JButton("Black");
            Dimension dimButton = blackButton.getPreferredSize();
            
            whiteButton.setBounds(gameDimension.width + 20, 10, dimButton.width + 20 , dimButton.height);
            blackButton.setBounds(gameDimension.width + 20, 20 + dimButton.height, dimButton.width + 20 , dimButton.height);
            
            whiteButton.setPreferredSize(new Dimension(dimButton.width + 20, dimButton.height));
            blackButton.setPreferredSize(new Dimension(dimButton.width + 20, dimButton.height));
            
            bigPanel.add(whiteButton);
            bigPanel.add(blackButton);
            bigPanel.add(player.gameControl);
            bigPanel.setPreferredSize(new Dimension(gameDimension.width + whiteButton.getWidth() + 50, gameDimension.height+50));
            bigPanel.setSize(new Dimension(gameDimension.width + whiteButton.getWidth(), gameDimension.height));
            
            parentFrame = (JFrame) SwingUtilities.getWindowAncestor(serverListPanel);
            parentFrame.add(bigPanel);
            parentFrame.setSize(player.gameControl.getWidth() + whiteButton.getWidth() + 50,parentFrame.getJMenuBar().getHeight() + player.gameControl.getHeight() + 50);
            parentFrame.setPreferredSize(new Dimension(player.gameControl.getWidth() + whiteButton.getWidth() + 20,parentFrame.getJMenuBar().getHeight() + player.gameControl.getHeight() + 30));
            Dimension frameDimension = parentFrame.getSize();
            parentFrame.setPreferredSize(frameDimension);
            parentFrame.setSize(frameDimension);
            parentFrame.pack();
            player.gameControl.isBlock = true;
            
            JOptionPane.showMessageDialog(parentFrame, "Choose your colour.");
            
            whiteButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    blackButton.setEnabled(false);
                    whiteButton.setEnabled(false);
                    player.setColour(false);                     
                    player.start();
                }
            });
            
            blackButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    blackButton.setEnabled(false);
                    whiteButton.setEnabled(false);
                    player.setColour(true);     
                    player.start();
                }
            });
        }
        catch (IOException ex) 
        {
            JOptionPane.showMessageDialog(parentFrame, "The server has disappear.");
            JPanel exceptionPane = new JPanel();
            parentFrame.setContentPane(exceptionPane);
            parentFrame.repaint();            
        }
        
    }
    
}
