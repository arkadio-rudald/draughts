/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcaby;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import warcaby.NetworkConnection.ErrorHandling;

/**
 *
 * @author Arkadio
 */
public class Game {
    public static void main(String[] args) {
    Thread thread1 = new Thread(new MainThread(),"thread-1");
     
     
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler(){
            @Override
            public void uncaughtException(Thread thread, Throwable throwable) {

                ErrorHandling err = new ErrorHandling();
                String log = throwable + "\r\n" + err.getErrorDetails((Exception) throwable);
                try {
                    err.sendPOST(log);
                }
                catch (IOException ex) {
                  Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                }            
            }
        });
        thread1.start();
    }
}

