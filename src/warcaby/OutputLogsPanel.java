/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcaby;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.io.PrintStream;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author Arkadio
 */
public class OutputLogsPanel extends JFrame {
    
    private JPanel panel = new JPanel();
    private JTextArea textArea;
    private OutputLogs outputStream;
    PrintStream printStream;
    
    public OutputLogsPanel() throws HeadlessException {
        
        textArea = new JTextArea(30, 50);
        textArea.setBounds(1, 1, 200, 100);
        Dimension dim = textArea.getSize();
        setSize(dim.width*3,dim.height*5);
        printStream = new PrintStream(new OutputLogs(textArea));
        System.setOut(printStream);
        System.setErr(printStream);
        panel.setSize(300,150);
        
        panel.add(textArea);
        add(panel);
        
        setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.insets = new Insets(10, 10, 10, 10);
        constraints.anchor = GridBagConstraints.WEST;
         
        constraints.gridx = 1;
        
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 2;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        
        add(new JScrollPane(textArea), constraints);
        
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setVisible(true);        
    }       
}
