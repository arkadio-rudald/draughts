/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcaby;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;

/**
 *
 * @author Arkadio
 */
public class CheckerBoard extends JPanel{
    public List <Field> checkerboard;
    
    private int boardSize;
    private int fieldSize;
   
    
    public CheckerBoard(int boardSize, int fieldSize)
    {
        setSize(boardSize*fieldSize, boardSize*fieldSize);
        this.boardSize = boardSize;
        this.fieldSize = fieldSize;
        createBoard();
    }
    
    public List <Field> getBoard()
    {
        return checkerboard;
    }
    
    public void createBoard()
    {
        checkerboard = new ArrayList<Field>();
        for(int i = 0; i < boardSize; i++)
        {
            for(int j = 0; j < boardSize; j++)
            {
                Field field = new Field();
                field.setId((i*boardSize)+j);
                field.setX1(j*fieldSize);
                field.setY1(i*fieldSize);
                field.setX2(((j+1)*fieldSize)-1);
                field.setY2(((i+1)*fieldSize)-1);
                
                int fieldsChangeably;
                if(i % 2 == 1)
                    fieldsChangeably = 1;
                else
                    fieldsChangeably = 0;
                if(j % 2 == fieldsChangeably)
                {
                    field.setFree(false);
                    field.setForbidden(true);
                }
                else
                {
                    field.setFree(true);
                    field.setForbidden(false);
                }
                checkerboard.add(field);
            }
        }
    }
    
    public void drawBoard(Graphics g)
    {        
        int mod2;
        mod2 = 0;
       
        for(int i = 2; i<(boardSize-1)*fieldSize+3 ; i=i+fieldSize)
        {
            for(int j = 2; j<(boardSize-1)*fieldSize+3; j=j+fieldSize)
            {
                if(mod2 % 2 == 0)
                {
                   g.setColor(Color.white);
                   mod2++;
                   g.fillRect(i, j, i+fieldSize, j+fieldSize);
                }
                else
                {
                   g.setColor(Color.black);
                   mod2++;
                   g.fillRect(i, j, i+fieldSize, j+fieldSize);
                }                                          
            }
            mod2++;
        }
        drawFieldNumber(g);
        drawFieldForbidden(g);
        drawFieldFree(g);
        g.setColor(Color.gray);
        g.drawRect(0, 0, (boardSize*fieldSize) - 1, (boardSize*fieldSize) - 1);
    }
    
    public void drawFieldNumber(Graphics g)
    {
        g.setColor(Color.red);
        for(Field field: checkerboard)
            g.drawString(String.valueOf(field.getId()), field.getX1() + 10, field.getY2());        
    }
    
    public void drawFieldFree(Graphics g)
    {
        g.setColor(Color.gray);
        for(Field field: checkerboard)
        {
            String text = null;
            if(field.isFree())
                text = "T";
            else
                text = "F";
            g.drawString(text, field.getX1() + fieldSize-20, field.getY1()+20);
        }        
    }
    
    public void drawFieldForbidden(Graphics g)
    {
        g.setColor(Color.green);
        
        for(Field field: checkerboard)
        {
            String text = null;
            if(field.isForbidden())
                text = "T";
            else
                text = "F";
            g.drawString(text, field.getX1() + 15 , field.getY1()+ 20);
        }        
    }

    public int getBoardSize() {
        return boardSize;
    }

    public void setBoardSize(int boardSize) {
        this.boardSize = boardSize;
    }

    public int getFieldSize() {
        return fieldSize;
    }

    public void setFieldSize(int fieldSize) {
        this.fieldSize = fieldSize;
    }
}
